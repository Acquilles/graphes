/// Fonction de tests des fonctions basiques (lecture, MCA, PTA, acceptation) du graphe

#ifndef test_graphe_H_
#define test_graphe_H_

#include "NGraph.hpp"
#include "utiles.h"

typedef unsigned int vertex;
typedef char lettre;
typedef typename NGraph::tGraph<vertex,lettre> Graph;

bool test_graphe(std::string positif_file)
{
	std::cerr << "-------- TEST NGraph --------" << std::endl;
//><<<<<<<<<<<<<<<<<<<<<<<<<<<<< Création du MCA >>>>>>>>>>>>>>>>>>>>>>>>>>;
	double debut, fin;
    Graph::echantillon positif = get_echantillon(positif_file);

    debut = chrono();
    Graph MCA = build_MCA<vertex,lettre>(positif);
    fin = chrono();
    std::cout << "MCA : " << fin - debut << std::endl;

//><<<<<<<<<<<<<<<<<<<<<<<<<<<<< CREATION PTA >>>>>>>>>>>>>>>>>>>>>>>>>>
    debut = chrono();
    Graph PTA = MCA.get_PTA();
    fin = chrono();
    std::cout << "PTA : " << fin - debut << std::endl;

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<  Écriture >>>>>>>>>>>>>>>>>>>>>>>>>>>>
    MCA.write_dot("MCA.dot","MCA");
    PTA.write_dot("PTA.dot","PTA");

	std::cerr << "-------- FIN  NGraph --------" << std::endl;

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<  FUSION  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 	MCA.get_quotient(1,5).get_quotient(4,0).write_dot("MCA.dot","quotient",false);

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<< RANDOM >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	Graph AL = build_random_char<vertex,lettre>(5,10,14,2);
	AL.write_dot("random.dot","RAND");
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<< ACCEPTATION >>>>>>>>>>>>>>>>>>>>>>>>>>>>

    if(MCA.complet(positif) and PTA.complet(positif))
        return true;
    return false;
}

#endif
