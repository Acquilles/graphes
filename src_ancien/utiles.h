#ifndef utiles_H_
#define utiles_H_


#include <time.h>
#include <sys/time.h>
#include <vector>
#include <sstream>
#include <fstream>
#include <iostream>
#include "NGraph.hpp"

/// Obtient le temps actuel
double chrono()
{
	struct timeval tmp_time;
	gettimeofday(&tmp_time, NULL);
	return tmp_time.tv_sec + (tmp_time.tv_usec * 1.0e-6L);
}

/// Retourne la liste des mots d'un fichier
std::vector<std::string> get_string_mots(std::string filename)
{
    std::vector<std::string> liste_mots;

    std::ifstream filemot(filename);
    std::string mot("");
    while(std::getline(filemot,mot))
    {
        liste_mots.push_back(mot);
    }
    return liste_mots;
}

/// Retourne l'échantillon MOTS UNIQUE d'un fichier
std::set<std::vector<char>> get_echantillon(std::string filename)
{
    std::set<std::vector<char>> liste_mots;

    std::ifstream filemot(filename);
    std::string m("");
    while(std::getline(filemot,m))
    {
        liste_mots.insert(std::vector<char>(m.begin(),m.end()));
    }
    return liste_mots;
}

/// Retourne l'échantillon d'un fichier
std::vector<std::vector<char>> get_echantillon_mult(std::string filename)
{
    std::vector<std::vector<char>> liste_mots;

    std::ifstream filemot(filename);
    std::string m("");
    while(std::getline(filemot,m))
    {
        liste_mots.push_back(std::vector<char>(m.begin(),m.end()));
    }
    return liste_mots;
}

std::ostream& operator<<(std::ostream&os,const std::vector<char>&m)
{
	for(const char & l :  m)
	{
		os << l ;
	}
	return os;
}

std::ostream& operator<<(std::ostream&os,const std::set<std::vector<char>>&e)
{
	for(const std::vector<char> & m :  e)
	{
		os << m << std::endl;
	}
	return os;
}


#endif
