template<typename T,typename C>
bool tGraph<T,C>::accepte(const typename tGraph<T,C>::mot& acceptation)const
{
    return accepte(begin(),acceptation,0);
}

template<typename T,typename C>
bool tGraph<T,C>::accepte(const_iterator pa, const typename tGraph<T,C>::mot& acceptation,size_t i)const
{
    if(i == acceptation.size())
    {
        return is_final(node(pa));
    }

    vertex_set a_out = out_neighbors(pa);
    for (typename vertex_set::iterator pb = a_out.begin(); pb!=a_out.end(); pb++)
    {
        if(is_lettre_in(node(pa),node(pb),acceptation[i]))
        {
            if(accepte(find(*pb),acceptation,i+1))
            {
                return true;
            }
        }
    }
    return false;
}

template<typename T,typename C>
bool tGraph<T,C>::complet(const typename tGraph<T,C>::echantillon& positif)const
{
    for(const typename tGraph<T,C>::mot& m : positif)
    {
        if(!accepte(m))
        {
            return false;
        }
    }
    return true;
}

template<typename T,typename C>
bool tGraph<T,C>::correct(const typename tGraph<T,C>::echantillon& negatif)const
{
    for(const typename tGraph<T,C>::mot& m :negatif)
    {
        if(accepte(m))
        {
            return false;
        }
    }
    return true;
}

template<typename T,typename C>
bool tGraph<T,C>::coherent(const typename tGraph<T,C>::echantillon& positif,const typename tGraph<T,C>::echantillon& negatif)const
{
    return complet(positif) and correct(negatif);
}
