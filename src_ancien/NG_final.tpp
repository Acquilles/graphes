template<typename T,typename C>
void tGraph<T,C>::set_final(const vertex& v)
{
    set_final(find(v));
}

template<typename T,typename C>
void tGraph<T,C>::set_final(tGraph<T,C>::iterator pv)
{
    if(F_.find(node(pv)) == F_.end())
    {
        F_.insert(node(pv));
    }
}

template<typename T,typename C>
void tGraph<T,C>::erase_final(const vertex &v)
{
    if(F_.find(v) != F_.end())
    {
            F_.erase(v);
    }
}

template<typename T,typename C>
bool tGraph<T,C>::is_final(const vertex& v)const
{
    return F_.find(v) != F_.end();
}
