#ifndef _VERFREQ_H_
#define _VERFREQ_H_
template <typename T>
class VerFreq
{
private:
    /// Numéro de VerFreq
    T k;
    /// Fréquence
    unsigned int f;
    /// Nombre de fin
    unsigned int e;
public:
    VerFreq():VerFreq(0){}
    VerFreq(T num):k(num),f(0),e(0){}
    T get_k()const{return k;}
    std::string print()const{std::ostringstream s;s << k<<'['<<f<<','<<e<<']';return s.str();}
    void increase_f(unsigned int i){f+=i;}
    void increase_e(unsigned int i){e+=i;}
    void e_equals_f(){e=f;}
    void operator=(const T num)
    {
        k = num;
    }
    VerFreq& operator++()
    {
        k++;
        e =0;
        f = 0;
        return *this;
    }

    VerFreq operator++(int)
    {
        VerFreq tmp = *this;
        ++*this;
        return tmp;
    }
};

template <typename T>
std::ostream &operator<<(std::ostream&os,const VerFreq<T>& v)
{
    os << v.print();
    return os;
}

template <typename T>
bool operator<(const VerFreq<T>&a,const VerFreq<T>&b)
{
    return a.get_k() < b.get_k();
}

template <typename T>
bool operator==(const VerFreq<T>&a,const VerFreq<T>&b)
{
    return a.get_k()== b.get_k();
}

template <typename T>
bool operator!=(const VerFreq<T>&a,const VerFreq<T>&b)
{
    return a.get_k()!= b.get_k();
}

#endif
