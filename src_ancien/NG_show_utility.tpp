template<typename T,typename C>
void tGraph<T,C>::write_dot(std::string filename,std::string name,bool erase)const
{
    /// - Si erase est activé, on écrase le fichier
    std::ofstream file;
    if(erase)
        file.open(filename);
    else
        file.open(filename, std::ios::app);

    file << "digraph{\n";
    file << "label=\""<<name<<"\";\n";
    file << "labelloc = \"t\";\n";
    //file << node(begin()) << " [label = "<<name<<"]" << std::endl;
    for (const_iterator p = begin(); p != end(); p++)
    {
        if (out_neighbors(p).size() == 0  &&
            in_neighbors(p).size() == 0)
        {
          file << "\""<<write(p) << "\";\n";
        }

        if(is_final(node(p)))
        {
            file <<"\""<< write(p) << "\" [shape = doublecircle]" << std::endl;
        }
    }
    for (const_iterator p = begin(); p != end(); p++)
    {
        const vertex_set &p_out = out_neighbors(p);
        vertex from = node(p);
        for (typename vertex_set::iterator q = p_out.begin();q != p_out.end(); q++)
        {
             for(const lettre&l : get_lettres(from,*q))
                file << "\"" << write(from) << "\"->\"" << write(node(q)) << "\"[label ="<<write_lettre(l)<<"] ;\n";
        }
    }
    file << "}\n";
    file.close();
}

template<typename T,typename C>
std::ostream & operator<<(std::ostream &s, const typename tGraph<T,C>::vertex_set &vs)
{
    for(const typename tGraph<T,C>::vertex & v : vs)
    {
        s << v << "  ";
    }
    s << std::endl;
    return s;
}
