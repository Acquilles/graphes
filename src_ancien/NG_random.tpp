/// Renvoie tous les échantillons de taille 10
template<typename T,typename  C>
std::pair<typename tGraph<T,C>::echantillon,typename tGraph<T,C>::echantillon> tGraph<T,C>::pos_neg(size_t tmax,const T& v , const typename tGraph<T,C>::mot& m)const
{
    typedef typename tGraph<T,C>::mot mot;
    typedef typename tGraph<T,C>::echantillon echantillon;
    // On complète notre dictionnaire avec le mot accepté ou refusé
    std::pair<echantillon,echantillon> pn;
    if(is_final(v))
    {
        pn.first.insert(m);
    }
    else
    {
        pn.second.insert(m);
    }

    if(tmax != 0)
    {
        for(const T& vo : out_neighbors(v))
        {
            for(const C&l : get_lettres(v,vo))
            {
                mot next_mot(m);
                next_mot.push_back(l);
                std::pair<echantillon,echantillon> next_pn = pos_neg(tmax-1,vo,next_mot);

                // Ajout des positifs :
                for(const typename tGraph<T,C>::mot& mn : next_pn.first)
                {
                    pn.first.insert(mn);
                }
                // Ajout des négatifs :
                for(const typename tGraph<T,C>::mot& mn : next_pn.second)
                {
                    pn.second.insert(mn);
                }
            }
        }
    }
    return pn;
}

template<typename T,typename  C>
/// Renvoie des échantillons aléatoire
std::pair<typename tGraph<T,C>::echantillon,typename tGraph<T,C>::echantillon> tGraph<T,C>::pos_neg_alea(size_t chance ,size_t tmax ,const T& v , const typename tGraph<T,C>::mot& m)const
{
    typedef typename tGraph<T,C>::mot mot;
    typedef typename tGraph<T,C>::echantillon echantillon;
    // On complète notre dictionnaire avec le mot accepté ou refusé
    std::pair<echantillon,echantillon> pn;
    if(is_final(v))
    {
        pn.first.insert(m);
    }
    else
    {
        pn.second.insert(m);
    }

    if((std::rand()%(tmax*tmax) > 1) and tmax!=0)
    {
        for(const T& vo : out_neighbors(v))
        {
            for(const C&l : get_lettres(v,vo))
            {
                mot next_mot(m);
                next_mot.push_back(l);
                std::pair<echantillon,echantillon> next_pn = pos_neg_alea(chance,tmax-1,vo,next_mot);

                // Ajout des positifs :
                for(const typename tGraph<T,C>::mot& mn : next_pn.first)
                {
                    pn.first.insert(mn);
                }
                // Ajout des négatifs :
                for(const typename tGraph<T,C>::mot& mn : next_pn.second)
                {
                    pn.second.insert(mn);
                }
            }
        }
    }
    return pn;
}
