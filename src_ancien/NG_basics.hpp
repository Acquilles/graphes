#ifndef basics_H_
#define basics_H_

#include "NGraph.hpp"

template<typename T,typename C>
typename NGraph::tGraph<T,C> build_MCA(const typename NGraph::tGraph<T,C>::echantillon_mult& positif)
{
    typename NGraph::tGraph<T,C> MCA(positif);
    return MCA;

}

template<typename T,typename C>
typename NGraph::tGraph<T,C> build_MCA(const typename NGraph::tGraph<T,C>::echantillon& positif)
{
    typename NGraph::tGraph<T,C>::echantillon_mult em;
    for(const typename NGraph::tGraph<T,C>::mot& m : positif)
        em.push_back(m);

    typename NGraph::tGraph<T,C> MCA(em);
    return MCA;

}

template<typename T,typename C>
typename NGraph::tGraph<T,C> build_RPNI(const typename NGraph::tGraph<T,C>::echantillon& positif, const typename NGraph::tGraph<T,C>::echantillon&negatif,std::string filename = "RPNI")
{
    typename NGraph::tGraph<T,C> A = build_MCA<T,C>(positif);
    A.write_dot(filename,"MCA",true);
    A.to_PTA();
    A.write_dot(filename,"PTA",false);
    A.to_RPNI_naive(negatif,filename);
    return A;
}



template<typename T,typename C>
typename NGraph::tGraph<T,C>::lettre random_lettre(const typename NGraph::tGraph<T,C>::mot& alphabet)
{
    return alphabet.at(std::rand() % alphabet.size());
}

template<typename T,typename C>
typename NGraph::tGraph<T,C>::vertex random_vertex(size_t nb_vertices)
{
    T v;
    size_t stop = std::rand() % nb_vertices;
    for (size_t i = 0; i < stop; i++) {
        v++;
    }
    return v;
}

template<typename T,typename C>
typename NGraph::tGraph<T,C> build_random(const typename NGraph::tGraph<T,C>::mot& alphabet,size_t nb_vertices,size_t nb_edges, size_t nb_finaux)
{
    typename NGraph::tGraph<T,C> A;
    T v;
    A.insert_vertex(v);
    for (size_t i = 0; i < nb_edges; i++) {
        A.insert_edge_lettre(A.random_vertex(),random_vertex<T,C>(nb_vertices),random_lettre<T,C>(alphabet));
    }

    for (size_t i = 0; i < nb_finaux; i++) {
        A.set_final(A.random_vertex());
    }
    A.determinize();
    return A;
}

template<typename T,typename C>
typename NGraph::tGraph<T,C> build_random_char(size_t nb_lettres,size_t nb_vertices,size_t nb_edges, size_t nb_finaux)
{
    typename NGraph::tGraph<T,C>::mot alphabet;
    C c;
    for (size_t i = 0; i < nb_lettres; i++) {
        alphabet.push_back(c++);
    }
    return build_random<T,C>(alphabet,nb_vertices,nb_edges,nb_finaux);
}

#endif
