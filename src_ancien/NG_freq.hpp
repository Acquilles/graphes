#ifndef NGFREQ_H_
#define NGFREQ_H_

/// file NG_freq.hpp Définit la classe et les fonctions tGfreq

#include "NGraph.hpp"
#include "VerFreq.hpp"
#include <math.h>

using namespace NGraph;
typedef unsigned int T;
typedef char C;

/// class Un graphe comptant les fréquences de passage par les états, pour l'inférence grammaticale stochastique.
// template<typename T, typename C>
class tGfreq : public tGraph<T,C>
{
public:
    typedef T vertex;
    typedef C lettre;
    typedef std::map<vertex,std::pair<unsigned int, unsigned int>> dic_occurences;
private:
    /// Les occurences de passages et d'arrivée sur chaque état
    dic_occurences O_;
public:
    typedef typename NGraph::tGraph<T,C> tG;

    tGfreq():tG(){}

    /// Augmentation de l'occurence de v de i
    bool increase_o(const vertex& v, unsigned int i = 1)
    {
        if(O_.find(v)!=O_.end())
            O_[v].first +=i;
            return true;
        return false;
    }

    /// Augmentation du nombre de mots finissants en v de i
    bool increase_f(const vertex& v, unsigned int i = 1)
    {
        if(O_.find(v)!=O_.end())
            O_[v].second +=i;
            return true;
        return false;
    }

    /// Ajout de l'initialisation des occurences de v à 0,0
    bool insert_vertex(const vertex &v)
    {
        if(tG::insert_vertex(v))
            O_[v] = std::make_pair(0,0);
            return true;
        return false;
    }

    /// Retournes les occurences de v
    unsigned int get_o(const vertex&v)const
    {
        if(O_.find(v)!=O_.end())
            return O_.at(v).first;
        return 0;
    }

    /// Retournes le nombre de finaux de v
    unsigned int get_f(const vertex&v)const
    {
        if(O_.find(v)!=O_.end())
            return O_.at(v).second;
        return 0;
    }

    /// Ajout de l'affichage des occurrences du vertex
    virtual std::string write(const vertex&v)const
    {
        std::ostringstream s;
        s << v << '[' << get_o(v) << ',' << get_f(v) << ']';
        return s.str();
    }

    virtual std::string write_lettre(const lettre&l)const
    {
        std::ostringstream s;
        s << l;
        return s.str();
    }
    /// Augmente les occurrences de a de celles de b
    bool sum_freq(const vertex&a,const vertex&b)
    {
        if(increase_o(a,get_o(b)))
            if(increase_f(a,get_f(b)))
                return true;
        return false;
    }

    /// Ajout de la construction  du dictionnaire des occurrences
    tGfreq(const typename tG::echantillon_mult& positif)
    {
        T compteur(0);
        T initial(0);
        compteur++;
        insert_vertex(initial);
        for(const mot& m : positif)
        {
            increase_o(initial);
            if(m.size()==0)
            {
                increase_f(initial);
                tG::set_final(initial);
            }
            else
            {
                tG::insert_edge_lettre(initial,compteur,m[0]);
                increase_o(compteur);
                for (size_t i = 1; i < m.size();i++)
                {
                    tG::insert_edge_lettre(compteur++,compteur,m[i]);
                    increase_o(compteur);
                }
                increase_f(compteur);
                tG::set_final(compteur++);
            }
        }
    }

    /// Ajout de la somme des occurences dans la fusion
    virtual void fusion_in_lettre(iterator pa, iterator pb, bool removepb = true)
    {
        if(removepb)
            sum_freq(node(pa),node(pb));
        tG::fusion_in_lettre(pa,pb,removepb);
    }

    /// Ajout de la somme des occurences dans la fusion
    virtual void fusion_out_lettre(iterator pa, iterator pb, bool removepb = true)
    {
        if(removepb)
            sum_freq(node(pa),node(pb));
        tG::fusion_out_lettre(pa,pb,removepb);
    }

    /// Renvoie l'ALERGIA
    // https://grfia.dlsi.ua.es/repositori/grfia/pubs/57/icgi1994.pdf
    void to_ALERGIA(double alpha = 0.05,std::string filename = "")
    {
        // Vecteur des états :
        std::vector<T> etats;
        for(const_iterator pv = begin();pv!=end();pv++)
        {
            etats.push_back(node(pv));
        }

        // Essayer chaque possibilité de fusion
        for (size_t i = 0; i < etats.size(); i++)
        {
            for (size_t j = 0; j < i; j++)
            {
                if(includes_vertex(etats[i]) and includes_vertex(etats[j]))
                {
                    if(compatible_ALERGIA(etats[j],etats[i],alpha))
                    {
                        fusion_deterministe(etats[j],etats[i]);
                        write_ALERGIA(etats[j],etats[i],filename);
                    }
                }
            }
        }
    }

    /// Écrit l'étape de fusion d'ALERGIA dans le filename
    void write_ALERGIA(const vertex&a,const vertex&b, std::string filename)
    {
        if(filename != "")
        {
            std::ostringstream nom;
            nom <<'{'<< a << ","<< b << "}";
            write_dot(filename,nom.str(),false);
        }
    }

    /// Indique si deux vertex sont compatibles pour la fusion au sens de l'AERGIA
    bool compatible_ALERGIA(const vertex&a,const vertex&b, double alpha = 0.05)const
    {
        // DÉTERMINISTE !!!!!!!!!!!!
        if(different_Hoeffding(get_f(a),get_o(a),get_f(b),get_o(b),alpha))
            return false;

        // Réunir toutes les letres qui partent des deux vertex
        std::set<lettre> lettres;
        for(const vertex& aout : out_neighbors(a))
            for(const lettre&l : get_lettres(a,aout))
                lettres.insert(l);
        for(const vertex& bout : out_neighbors(b))
            for(const lettre&l : get_lettres(b,bout))
                lettres.insert(l);

        // Tester leur similarité
        for(const lettre& l : lettres)
        {
            // Niquer les 0 parce que ninon pas de récursif :
            unsigned int fa = frequence_lettre(a,l),fb = frequence_lettre(b,l);
            if(different_Hoeffding(fa,get_o(a),fb,get_o(b),alpha))
                return false;
            if(fa!=0 and fb != 0)
                if(!compatible_ALERGIA(get_vertex_lettre(a,l),get_vertex_lettre(b,l),alpha))
                    return false;
        }

        return true;
    }

    /// Renvoie la fréquence de passage de la lettre l suivant le vertex a
    unsigned int frequence_lettre(const vertex& a, const lettre &l)const
    {
        /// DÉTERMINISTE !!!!!!!!!!!!
        for(const vertex& aout : out_neighbors(a))
            if(is_lettre_in(a,aout,l))
                return get_o(aout);
        return 0;
    }

    /// Renvoie le premier vertex descendant de a sur une lettre l
    vertex get_vertex_lettre(const vertex& a, const lettre &l)const
    {
        /// DÉTERMINISTE !!!!!!!!!!!!
        for(const vertex& aout : out_neighbors(a))
            if(is_lettre_in(a,aout,l))
                return aout;
        return vertex();
    }

    /// Renvoie faux si on est sur au sens de Hoeffding à un niveau 1-\alpha que les deux fréquences sont différentes
    bool different_Hoeffding(unsigned int fa,unsigned int na,unsigned int fb,unsigned int nb,double alpha = 0.05)const
    {
        return abs(fa/na-fb/nb) > sqrt(log(2/alpha)*(1/sqrt(na)+1/sqrt(nb))/2);
    }
};

#endif
