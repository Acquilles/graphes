// Projet abandonné
vertex new_vertex()
{
    vertex nouveau(num_nodes());
    return nouveau;
}

tGraph get_ECGI(const std::vector<mot> &liste_mots)
{
    tGraph G;
    if(liste_mots.size()>0)
    {
        // Création du graphe orgininal
        mot m1 = liste_mots[0];
        vertex compteur = 0;
        G.insert_edge_lettre(compteur++,compteur,m1[0]);
        for (const lettre& l : m1) {
            G.insert_edge_lettre(compteur++,compteur,l);
            compteur++;
        }
        G.set_final(compteur);

        // Step ECGI
        for(std::vector<mot>::const_iterator pm = liste_mots.begin()+1;pm!=liste_mots.end();pm++)
        {
            G.step_ECGI(*pm);
        }
    }
    return G;
}

void step_ECGI(mot m)
{
    size_t i = 0;
    chemin opti(get_deriv_opti(m));
    for(const lettre& l: m)
    {
        edge current_edge = std::make_pair(opti[i],opti[i+1]);
        if(!includes_edge(current_edge))
        {
            insert_edge_lettre(current_edge,l);
        }
    }
}

chemin get_deriv_opti(mot m)
{
    // Chaque chemin possible et son score correspondant
    typename std::map<vertex,std::pair<chemin,size_t>> state_to_chemins;

    // On initialise au chemin qui part du commencement avec un score nul
    chemin initial;
    initial.push_back(node(begin()));
    state_to_chemins[0] = std::make_pair(initial,0);

    // Pour chaque lettre :
    for(const lettre&l : m)
    {
        // On va constituer la nouvelle liste de chemins
        std::set<std::pair<chemin,size_t>> new_chemins;

        // On parcourt tous les chemins existants
        for(const std::pair<vertex,std::pair<chemin,size_t>>&  v_to_c_s: state_to_chemins)
        {
            // On ne récupère que le chemin et le score :
            std::pair<chemin,size_t> chemin_et_score = v_to_c_s.second;

            // Le dernier état et l'état suivant :
            vertex last_v = chemin_et_score.first.back();
            vertex next_v;

            // Si la taille est supérieure c'est que c'était une insertion
            // donc on arrête

            //On crée les 4 ajouts correspondants

            // # 1 chemin continuant


            // Voir si on trouve une transition de la bonne lettre
            bool trouver = false;
            for(const vertex & v: out_neighbors(last_v))
            {
                if(is_lettre_in(last_v,v,l))
                {
                    trouver = true;
                    next_v = v;
                }
            }

            // Si oui, on ajoute le nouveau chemin avec son score inchangé
            if(trouver)
            {
                chemin chemin_trouve(chemin_et_score.first);
                chemin_trouve.push_back(next_v);
                new_chemins.insert(std::make_pair(chemin_trouve,chemin_et_score.second));
            }

            // # 2 Suppression
            // On va passer dans chaque descendant de descendant pour ajouter un lien
            // for(const  vertex d: out_neighbors(last_v))
            // {
            //     (const & vertex d_d: out_neighbors(d))
            //     {
            //         new_chemins.insert()
            //     }
            // }

        }
        // On parcours tous les nouveaux chemins construits jusqu'à présent
        for(const typename std::pair<chemin,size_t> chemin_score : new_chemins)
        {
            vertex v = chemin_score.first.front();

            // Si l'état n'est pas attribué dans la table, on l'insère
            if(state_to_chemins.find(v)==state_to_chemins.end())
            {
                state_to_chemins[v] = std::make_pair(chemin_score.first,chemin_score.second);
            }
            // Sinon et le chemin associé à l'état a un score moins élevé, on l'insère
            else if(state_to_chemins[v].second > chemin_score.second)
            {
                state_to_chemins[v] = std::make_pair(chemin_score.first,chemin_score.second);
            }
        }
    }

    // On renvoie ensuite le chemin avec le score le plus bas
    vertex v_final = state_to_chemins.begin()->first;
    size_t min_final = state_to_chemins.begin()->second.second;
    for(typename std::pair<vertex,std::pair<chemin,size_t>> v_to_c_s  : state_to_chemins)
    {
        if(v_to_c_s.second.second < min_final)
        {
            min_final = v_to_c_s.second.second ;
            v_final = v_to_c_s.first;
        }
    }
    return state_to_chemins[v_final].first;
}    }
    }
    return state_to_chemins[v_final].first;
}
