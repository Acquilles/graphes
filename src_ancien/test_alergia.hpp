#include "NGraph.hpp"
#include "NG_basics.hpp"
#include "utiles.h"
#include "VerFreq.hpp"
#include "NG_freq.hpp"
#include "NG_prob.hpp"

using namespace NGraph;

void test_alergia()
{

    tGprob<unsigned int, char> rrr;
    rrr.insert_vertex(0,0.6);
    rrr.insert_vertex(1,0.0);
    rrr.insert_edge_lettre(0,0,'0',0.2);
    rrr.insert_edge_lettre(0,1,'1',0.2);
    rrr.insert_edge_lettre(1,1,'1',0.5);
    rrr.insert_edge_lettre(1,0,'0',0.5);
    rrr.write_dot("RANDOM.dot","RANDOM",true);
    std::ofstream fal("../data/alergia2.txt");
    for (size_t i = 0; i < 60; i++)
    {
        for(const char& c : rrr.random_mot())
        {
            fal << c ;
        }
        fal << std::endl;
    }


    tGfreq nvo(get_echantillon_mult("../data/alergia.txt"));
    nvo.write_dot("freq.dot","MCA");
    nvo.to_PTA();
    nvo.write_dot("freq.dot","PTA",false);
    nvo.fusion_deterministe(0,4);
    nvo.write_ALERGIA(0,4,"freq.dot");
    nvo.fusion_deterministe(0,9);
    nvo.write_ALERGIA(0,9,"freq.dot");
    nvo.fusion_deterministe(1,2);
    nvo.write_ALERGIA(1,2,"freq.dot");
    // nvo.to_ALERGIA(0.3,"freq.dot");
    // nvo.write_dot("freq.dot","Final",false);
}
