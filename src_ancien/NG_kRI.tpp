
template<typename T,typename C>
tGraph<T,C> tGraph<T,C>::get_kRI(size_t k)const
{
    tGraph R(*this);
    R.to_kRI(k);
    return R;
}

template<typename T,typename C>
void tGraph<T,C>::to_kRI(size_t k)
{
    to_PTA();
    while(true)
    {
        if (!step_kRI_c1())
            if(!step_kRI_c2a(k))
                if(!step_kRI_c2b(k))
                    return;
    }
}

template<typename T,typename C>
tGraph<T,C> tGraph<T,C>::get_kRI(size_t k,std::string filename)const
{
    tGraph<T,C> R(*this);
    R.to_kRI(k,filename);
    return R;
}

/// KRI avec la documentation
template<typename T,typename C>
void tGraph<T,C>::to_kRI(size_t k,std::string filename)
{
    write_dot(filename,"MCA");
    to_PTA();
    write_dot(filename,"PTA",false);
    bool notend = true;
    while(notend)
    {
        if(step_kRI_c1())
            write_dot(filename,"C1",false);
        else if(step_kRI_c2a(k))
            write_dot(filename,"C2a",false);
        else if(step_kRI_c2b(k))
            write_dot(filename,"C2b",false);
        else
            notend = false;

    }
    write_dot(filename,"Final",false);
}

template<typename T,typename C>
bool tGraph<T,C>::step_kRI_c1()
{
    return step_determinize();
}

template<typename T,typename C>
bool tGraph<T,C>::step_kRI_c2a(size_t k)
{
    vertex_set_set to_merge;
    to_merge.insert(get_bloc_kRIC2a(k));
    if(to_merge.begin()->size()>1)
    {
        return to_quotient(to_merge);
    }
    return false;
}

template<typename T,typename C>
bool tGraph<T,C>::step_kRI_c2b(size_t k)
{
    vertex_set_set to_merge;
    to_merge.insert(get_bloc_kRIC2b(k));
    if(to_merge.begin()->size()>1)
    {
        return to_quotient(to_merge);

    }
    return false;
}

template<typename T,typename C>
typename tGraph<T,C>::vertex_set tGraph<T,C>::get_bloc_kRIC2a(size_t k)const
{
    std::map<vertex,vertex> finaux;
    for(const vertex& f : F_)
    {
        finaux[f] = f;
    }
    return get_bloc_kRI_mergeable(finaux,k);
}

template<typename T,typename C>
typename tGraph<T,C>::vertex_set tGraph<T,C>::get_bloc_kRIC2b(size_t k)const
{
    vertex_set to_merge;

    /// - Pour chaque sommet du graphe :
    for (const_iterator p = begin(); p != end(); p++)
    {
        /// - - On réunit les ensembles des vertex parents associés à leur lettre dans lettre_to_va_to_vb
        vertex_set p_in = in_neighbors(p);
        std::map<lettre,std::map<vertex,vertex>> lettre_to_va_to_vb;
        for(const vertex& v : p_in)
        {
            set_lettre sl = get_lettres(v,node(p));
            for(const lettre & l : sl)
            {
                if(lettre_to_va_to_vb.find(l) == lettre_to_va_to_vb.end())
                {
                    lettre_to_va_to_vb[l] = std::map<vertex,vertex>();
                    lettre_to_va_to_vb[l][v] = v;
                }
                lettre_to_va_to_vb[l][v] =v;
            }
        }

        /// - - On tente d'obtenir les blocs k-RI fusionnables
        for (const std::pair<lettre,std::map<vertex,vertex>> &l_in : lettre_to_va_to_vb)
        {
            to_merge = get_bloc_kRI_mergeable(l_in.second,k);
            if(to_merge.size() > 1)
            {
                return to_merge;
            }
        }
    }
    /// - Sinon, on renvoie un truc vide
    return to_merge;
}

template<typename T,typename C>
typename tGraph<T,C>::vertex_set tGraph<T,C>::get_bloc_kRI_mergeable(const std::map<vertex,vertex>& to_who,size_t k)const
{
    /// - Si k = 0, on peut renvoyer les blocs de to_who
    if(k==0)
    {
        vertex_set to_merge;
        for(const std::pair<vertex,vertex>& vtov : to_who)
        {
            to_merge.insert(vtov.second);
        }
        return to_merge;
    }

    /// - On réunit les ensembles des associations vertex à vertex avec leur lettre dans lettre_to_va_to_vb
    std::map<lettre,std::map<vertex,vertex>> lettre_to_va_to_vb;
    for(const std::pair<vertex,vertex> & vtov : to_who)
    {
        vertex_set v_in = in_neighbors(vtov.first);
        for(const vertex &v_a : v_in)
        {
            set_lettre sl = get_lettres(v_a,vtov.first);
            for(const lettre & l : sl)
            {
                if(lettre_to_va_to_vb.find(l) == lettre_to_va_to_vb.end())
                {
                    lettre_to_va_to_vb[l] = std::map<vertex,vertex>();
                }
                lettre_to_va_to_vb[l][v_a] = vtov.second;
            }
        }
    }

    /// - On renvoie une exécution sur les ensembles ainsi construits si elle réussit
    vertex_set to_merge;
    for (const std::pair<lettre,std::map<vertex,vertex>> & to_who_l : lettre_to_va_to_vb)
    {
        to_merge = get_bloc_kRI_mergeable(to_who_l.second,k-1);
        if(to_merge.size() > 1)
        {
            return to_merge;
        }
    }
    return to_merge;
}
