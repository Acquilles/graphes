#ifndef test_RPNI_H_
#define test_RPNI_H_

#include "NGraph.hpp"
#include "utiles.h"
#include "NG_basics.hpp"

typedef unsigned int vertex;
typedef char lettre;
typedef  NGraph::tGraph<vertex,lettre> Graph;
typedef  std::vector<char> mot;
typedef  std::set<mot> echantillon;

bool test_RPNI(std::string filenamepos,std::string filenameneg,std::string filebuild ="")
{
	std::cerr << "-------- TEST RPNI --------" << std::endl;
	Graph::echantillon positif = get_echantillon(filenamepos);
	Graph::echantillon negatif = get_echantillon(filenameneg);
	Graph RPNI = build_RPNI<vertex,lettre>(positif,negatif,filebuild);
	std::cerr << "-------- FIN  RPNI --------" << std::endl;
	RPNI.write_dot("RPNI.dot","RPNI");
    return RPNI.coherent(positif,negatif);

}

bool test_RPNI_rand(size_t a, size_t v, size_t e, size_t f,size_t t)
{
	std::cerr << "-------- BUILD RAND --------" << std::endl;
	Graph AL = build_random_char<vertex,lettre>(a,v,e,f);
	AL.write_dot("random.dot","RAND");
	std::cerr << "-------- WRITE ECHA --------" << std::endl;
	std::pair<echantillon,echantillon> pn = AL.pos_neg_alea(t);
	std::ofstream fpos("../data/randpos.txt");
	std::ofstream fneg("../data/randneg.txt");
	fpos << pn.first;
	fneg << pn.second;
	fpos.close();fneg.close();
	return test_RPNI("../data/randpos.txt","../data/randneg.txt","");

}
#endif
