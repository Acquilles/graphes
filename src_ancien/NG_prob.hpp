#ifndef NGPROB_H_
#define NGPROB_H_

/// file NG_freq.hpp Définit la classe et les fonctions tGfreq

#include "NGraph.hpp"
#include "VerFreq.hpp"
#include <math.h>


std::ostream& operator<<(std::ostream&os,const typename tGraph<T,std::pair<C,double>>::lettre& l)
{
    os << "CACA";
    return os;
}
/// class Un graphe comptant les fréquences de passage par les états, pour l'inférence grammaticale stochastique.
template<typename T, typename C>
class tGprob : public tGraph<T,std::pair<C,double>>
{
public:
    typedef T vertex;
    typedef std::pair<C,double> lettre;
    typedef std::map<vertex,double> liste_proba;
private:
    /// Probabilité de rester sur l'état
    liste_proba PS_;
public:
    typedef typename NGraph::tGraph<T,std::pair<C,double>> tG;

    tGprob():tG(){}

    virtual std::string write_lettre(const lettre&l)const{
        std::ostringstream nom;
        nom << '"' << l.first << '(' << l.second << ')' << '"';
        return nom.str();
    }

    /// Ajout de l'initialisation des occurences de v à 0,0
    bool insert_vertex(const vertex &v,double prob_stay)
    {
        tG::insert_vertex(v);
        PS_[v] = prob_stay;
        return true;
    }


    /// Ajout de l'initialisation des occurences de v à 0,0
    void insert_edge_lettre(const vertex&a,const vertex&b,const C&c, double proba)
    {
        tG::insert_edge_lettre(a,b,std::make_pair(c,proba));
    }

    std::vector<C> random_mot()
    {
        std::vector<C> m;
        return random_mot(m,tG::node(tG::begin()));
    }

    std::vector<C> random_mot(std::vector<C>& m ,const vertex& a)
    {
        double alea = (double) std::rand()/RAND_MAX;
        for(const vertex& b : tG::out_neighbors(a))
        {
            for(const lettre& l : tG::get_lettres(a,b))
            {
                alea -= l.second;
                if(alea < 0)
                {
                    m.push_back(l.first);
                    return random_mot(m,b);
                }
            }
        }
        return m;
    }
    /// Retournes la proba de rester en v
    double get_prob_stay(const vertex&v)const
    {
        if(PS_.find(v)!=PS_.end())
            return PS_.at(v);
        return -1;
    }

    /// Ajout de l'affichage des occurrences du vertex
    virtual std::string write(const vertex&v)const
    {
        std::ostringstream s;
        s << v << '[' << get_prob_stay(v) <<']';
        return s.str();
    }


};

#endif
