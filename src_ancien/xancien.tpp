typedef tGraph<unsigned int> Graph;
typedef tGraph<int> iGraph;
typedef tGraph<std::string> sGraph;


template <class T>
std::vector<typename tGraph<T,C>::edge> tGraph<T,C>::edge_list() const
    {
        //std::vector<tGraph::edge> E(num_edges());
        std::vector<typename tGraph<T,C>::edge> E;

        for (typename tGraph::const_iterator p = begin(); p!=end(); p++)
        {
            const vertex &a = tGraph::node(p);
            const vertex_set &out = tGraph::out_neighbors(p);
            for (typename vertex_set::const_iterator t = out.begin();
                        t != out.end(); t++)
            {
                E.push_back( edge(a, *t));
            }
        }
        return E;
    }


template <typename T>
std::istream & operator>>(std::istream &s, tGraph<T,C> &G)
{
    std::string line;
    T v1, v2;
    typename tGraph<T,C>::line_type t;

    while (tGraph<T,C>::read_line(s, v1, v2, line, t))
    {
        if (t == tGraph<T,C>::VERTEX)
        {
            G.insert_vertex(v1);
        }
        else if (t == tGraph<T,C>::EDGE)
        {
            G.insert_edge(v1, v2);
        }
    }
    return s;
}

template <typename T>
std::ostream & operator<<(std::ostream &s, const tGraph<T,C> &G)
{
  for (typename tGraph<T,C>::const_node_iterator p=G.begin(); p != G.end(); p++)
  {
    const typename tGraph<T,C>::vertex_set &out = tGraph<T,C>::out_neighbors(p);
    typename tGraph<T,C>::vertex v = p->first;
    if (out.size() == 0 && tGraph<T,C>::in_neighbors(p).size() == 0)
    {
      // v is an isolated node
      s << v << "\n";
    }
    else
    {
       for ( typename tGraph<T,C>::vertex_set::const_iterator q=out.begin();
                q!=out.end(); q++)
           s << v << " " << *q << "\n";
    }
  }
  return s;
}
//
// template <typename T>
// void fusion_2_vertex(tGraph<T,C>::vertex_set_set& vss,const tGraph<T,C>::vertex&va,const tGraph<T,C>::vertex&vb)
// {
//     vert
//     for(const tGraph<T,C>::vertex_set & vs : vss)
//     {
//         for(const tGraph<T,C>::vertex & v : vs)
//         {
//             if (v==va)
//             {
//
//             }
//         }
//     }
// }
template <typename T>
void tGraph<T,C>::print() const
    {

       std::cerr << "# vertices: " <<  num_vertices()  << "\n";
       std::cerr << "# edges:    " <<  num_edges()  << "\n";

        for (const_iterator p=G_.begin();
              p != G_.end(); p++)
        {
          const vertex_set   &out =  out_neighbors(p);

          for (typename vertex_set::const_iterator q=out.begin();
                          q!=out.end(); q++)
              std::cerr << p->first << "  -->  " << *q << "\n";
        }
        std::cerr << std::endl;

    }

}


/// Déterministe !!!
