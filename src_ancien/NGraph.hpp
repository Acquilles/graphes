/*
*
* NGraph++ : a simple graph library
*
* Mathematical and Computational Sciences Division
* National Institute of Technology,
* Gaithersburg, MD USA
*
*
* This software was developed at the National Institute of Standards and
* Technology (NIST) by employees of the Federal Government in the course
* of their official duties. Pursuant to title 17 Section 105 of the
* United States Code, this software is not subject to copyright protection
* and is in the public domain. NIST assumes no responsibility whatsoever for
* its use by other parties, and makes no guarantees, expressed or implied,
* about its quality, reliability, or any other characteristic.
*
*/

#ifndef NGRAPH_H_
#define NGRAPH_H_

// version 4.2


#include <iostream>
#include <set>
#include <list>
#include <map>
#include <utility>      // for std::pair
#include <iterator>     // for inserter()
#include <vector>       // for exporting edge_list
#include <string>
#include <algorithm>
#include <sstream>      // for I/O << and >> operators
#include <fstream>
// #include "set_ops.hpp"
namespace NGraph
{

/* Les T (vertices) et les C (lettres) doivent posséder:
- Constructeur défaut
- Opérateurs :
    - <<
    - !=
    - ==
    - <
    - ++ (pour construction automatique)

*/
template <typename T,typename C>
class tGraph
{
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< TYPEDEF >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  public:
    typedef C lettre; // <<<< Ach
    typedef std::vector<lettre> mot;  // <<<< Ach
    typedef std::set<mot> echantillon;       // <<<< Ach
    typedef std::vector<mot> echantillon_mult;  // <<<< Ach
    typedef std::set<lettre> set_lettre;  // <<<< Ach

    typedef T vertex;
    typedef T value_type;
    typedef std::tuple<vertex,vertex> edge;
    typedef std::map<edge,set_lettre> lien_lettres;
    typedef std::set<vertex> vertex_set;

    typedef std::set<vertex_set> vertex_set_set;  // <<<< Ach
    typedef typename std::vector<vertex> chemin; // <<<< Ach

    typedef std::set<edge> edge_set;
    typedef std::pair<vertex_set, vertex_set> in_out_edge_sets;
    typedef std::map<vertex, in_out_edge_sets>  adj_graph;

    typedef typename edge_set::iterator edge_iterator;
    typedef typename edge_set::const_iterator const_edge_iterator;

    typedef typename vertex_set::iterator vertex_iterator;
    typedef typename vertex_set::const_iterator const_vertex_iterator;

    typedef typename vertex_set_set::iterator vertex_set_iterator;  // <<<< Ach
    typedef typename vertex_set_set::const_iterator const_vertex_set_iterator;  // <<<< Ach

    enum line_type { VERTEX, EDGE, COMMENT, EMPTY};

    typedef typename vertex_set::iterator vertex_neighbor_iterator;
    typedef typename vertex_set::const_iterator vertex_neighbor_const_iterator;
    typedef typename adj_graph::iterator iterator;
    typedef typename adj_graph::const_iterator const_iterator;

    typedef iterator node_iterator;
    typedef const_iterator const_node_iterator;

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ATTIRBUTS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    private:

    adj_graph G_;
    lien_lettres L_; // <<<< Ach : Correspondances edges et lettres
    vertex_set F_; // <<<< Ach : Les états finaux
    unsigned int num_edges_;

    public:
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< BASIQUES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    unsigned int num_vertices() const { return G_.size(); }
    unsigned int num_nodes() const { return G_.size(); }
    unsigned int num_edges() const { return num_edges_; }

    iterator begin() { return G_.begin(); }
    const_iterator begin() const { return G_.begin(); }
    iterator end() { return G_.end(); }
    const_iterator end() const { return G_.end(); }

    void clear()
    {
      G_.clear();
      num_edges_ = 0;
    }

    vertex_neighbor_iterator out_neighbors_begin(const vertex &a)
    {
      return out_neighbors(a).begin();
    }

    vertex_neighbor_const_iterator out_neighbors_begin(const vertex &a) const
    {
      return out_neighbors(a).begin();
    }

    vertex_neighbor_iterator out_neighbors_end(const vertex &a)
    {
      return out_neighbors(a).end();
    }

    vertex_neighbor_const_iterator out_neighbors_end(const vertex &a) const
    {
      return out_neighbors(a).end();
    }

    iterator find(const vertex &a)
     {
        return G_.find(a);
     }

     const_iterator find(const vertex  &a) const
     {
        return G_.find(a);
     }

    const vertex_set &in_neighbors(const vertex &a) const
              { return (find(a))->second.first;}
          vertex_set &in_neighbors(const vertex &a)
              { return (G_[a]).first; }

    const vertex_set &out_neighbors(const vertex &a)const
                {return find(a)->second.second;}
          vertex_set &out_neighbors(const vertex &a)
                {return G_[a].second; }


     unsigned int in_degree(const vertex &a) const
     {
        return in_neighbors(a).size();
     }

     unsigned int out_degree(const vertex &a) const
     {
        return out_neighbors(a).size();
     }

     unsigned int degree(const vertex &a) const
     {
        const_iterator p = find(a);
        if (p == end())
          return 0;
        else
          return degree(p);
     }


    bool isolated(const vertex &a) const
    {
        const_iterator p = find(a);
        if ( p != end() )
          return  isolated(p) ;
        else
            return false;
    }
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// <<<<<<<<<<<<<<<<<<<<<<<<<<< CONSTRUCTEURS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    tGraph(): G_(), num_edges_(0){}
    tGraph(std::istream &s): G_(), num_edges_(0)
    {
      s >> *this;
    }

    tGraph(const tGraph &B) : G_(B.G_), L_(B.L_), F_(B.F_),num_edges_(B.num_edges_){}
    tGraph(const edge_set &E)
    {
      for (typename edge_set::const_iterator p = E.begin();
                      p != E.end(); p++)
        insert_edge(*p);
    }

    void clone(const tGraph &B){
        G_ =B.G_; L_ =B.L_; F_ =B.F_;num_edges_ =B.num_edges_;
    }

    // CONSTRUIT LE MCA
    tGraph(const echantillon_mult&positif)
    {
        vertex compteur = vertex();
        vertex initial = vertex();
        compteur++;
        insert_vertex(initial);
        for(const mot& m : positif)
        {
            if(m.size()==0)
            {
                set_final(find(initial));
            }
            else
            {
                insert_edge_lettre(initial,compteur,m[0]);
                for (size_t i = 1; i < m.size();i++)
                {
                    insert_edge_lettre(compteur++,compteur,m[i]);
                }
                set_final(find(compteur++));
            }
        }
    }


        tGraph(const echantillon&positif)
        {
            vertex compteur = vertex();
            vertex initial = vertex();
            compteur++;
            insert_vertex(initial);
            for(const mot& m : positif)
            {
                if(m.size()==0)
                {
                    set_final(find(initial));
                }
                else
                {
                    insert_edge_lettre(initial,compteur,m[0]);
                    for (size_t i = 1; i < m.size();i++)
                    {
                        insert_edge_lettre(compteur++,compteur,m[i]);
                    }
                    set_final(find(compteur++));
                }
            }
        }

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ACHILLE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// https://hal.inria.fr/inria-00073241/document

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<< RPNI >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Retourne le graphe RPNI d'un PTA avec implémentation naïve
    void get_RPNI_naive(const echantillon& negatif,std::string filename = "")const;
    /// Convertit un PTA en RPNI avec implémentation naïve
    void to_RPNI_naive(const echantillon& negatif,std::string filename = "");

// <<<<<<<<<<<<<<<<<<<<<<Génération aléatoire >>>>>>>>>>>>>>>>>>>>>>>>>>>
/// Renvoie un vertex aléatoire
    vertex random_vertex()
    {
        size_t stop = std::rand() % num_vertices();
        size_t i = 0;
        for(const_iterator it = begin();it!=end();it++)
        {
            if(i++ >= stop)
            {
                return it->first;
            }
        }
        return begin()->first;
    }
    /// Renvoie tous les échantillons de taille 10
    std::pair<echantillon,echantillon> pos_neg(size_t tmax = 10,const vertex& v = vertex() , const mot& m = mot())const;
    /// Renvoie des échantillons aléatoire
    std::pair<echantillon,echantillon> pos_neg_alea(size_t chance = 2,size_t tmax = 7,const vertex& v = vertex() , const mot& m = mot())const;
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<< Cohérence >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Tous les échantillons positifs sont acceptés
    bool complet(const echantillon& positif)const;
    /// Tous les échantillons négatifs sont refusés
    bool correct(const echantillon& negatif)const;
    /// Complet et correct
    bool coherent(const echantillon& positif,const echantillon& negatif)const;

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<< ECGI Viterbi >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    // https://hal.archives-ouvertes.fr/hal-00390903/document
    //https://core.ac.uk/download/pdf/71029568.pdf
     // Finalement c'était très dur


// <<<<<<<<<<<<<<<<<<<<<<<<<<<<< ACCEPTATION >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    bool accepte(const mot& acceptation)const;

    bool accepte(const_iterator pa, const mot& acceptation,size_t i)const;

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<< k-RI>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    // https://hal.inria.fr/inria-00073241/document

    tGraph get_kRI(size_t k)const;
    void to_kRI(size_t k);
    tGraph get_kRI(size_t k,std::string filename)const;
    /// KRI avec la documentation
    void to_kRI(size_t k,std::string filename);
    /// Une étape de fusion k-RI au sens de la Condition 1
    bool step_kRI_c1();

    /// Une étape de fusion k-RI au sens de la Condition 2a
    bool step_kRI_c2a(size_t k);

    /// Une étape de fusion k-RI au sens de la Condition 2b
    bool step_kRI_c2b(size_t k);
    vertex_set_set get_bloc_kRI_c1()const;

    /// Obtenir les blocs k-RI fusionnables au sens de la Condition 2a
    vertex_set get_bloc_kRIC2a(size_t k)const;

    /// Obtenir les blocs k-RI fusionnables au sens de la Condition 2b
    vertex_set get_bloc_kRIC2b(size_t k)const;
    vertex_set get_bloc_kRI_mergeable(const std::map<vertex,vertex>& to_who,size_t k)const;

// <<<<<<<<<<<<<<<<<<<<<<<<<<< Éats finaux >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    void set_final(const vertex& v);

    void set_final(iterator pv);

    void erase_final(const vertex &v);

    bool is_final(const vertex& v)const;

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<< QUOTIENT>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Renvoi le quotient de deux éléments.
    tGraph get_quotient(const vertex&a,const vertex&b) const;

    // Quotient de deux éléments.
    bool to_quotient(const vertex&a,const vertex&b);

    /// Renvoi le quotient du graphe par pi
    tGraph get_quotient(const vertex_set_set& pi) const;

    /// Divise le graphe par pi, c-a-d regroupe les ensembles d'états que contient pi
    bool to_quotient(const vertex_set_set& pi);

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< PTA >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Renvoie les blocs à fusioner pour déterminiser un graphe
    vertex_set_set get_bloc_to_determinize()const;

    /// Déterminise le graphe
    void determinize()
    {
        while(step_determinize());
    }

    /// Avance d'un pas de déterminisme et renvoie vrai si il y a eu fusion
    bool step_determinize();

    /// Renvoie le PTA du Graphe
    tGraph get_PTA() const;
    /// Éxécute le PTA sur le graphe
    void to_PTA();

    /// Éxécute le liens des similaires et récursive sur ses nouveaux descendants
    void recursive_PTA(iterator pa);

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< FUSION >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Fusionne les deux états et déterminise le graphe
    void fusion_deterministe(const vertex& a, const vertex& b)
    {
        to_quotient(a,b);
        determinize();
    }
    /// Fusionne tous les états descendants ayant la même lettre sur leur edge
    bool fusion_similaires(const vertex&a);

    /// Fusionne tous les états descendants ayant la même lettre sur leur edge
    bool fusion_similaires(iterator pa);

    /// Insertion en pa des parent de pb et des lettres associées à leurs edges
    virtual void fusion_in_lettre(iterator pa, iterator pb, bool removepb = true);

    /// Insertion en pa des descendants de pb et des lettres associées à leurs edges
    virtual void fusion_out_lettre(iterator pa, iterator pb, bool removepb = true);

    /// Insertion en pa des descendants et parents de pb et des lettres associées à leurs edges
    virtual void fusion_lettre(iterator pa, iterator pb, bool removepb = true);

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Lettre >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Indique si la lettre est dans un vertice de l'un à l'autre
    bool is_lettre_in(const vertex& a, const vertex&b,const lettre& l)const;

    /// Renvoie la lettre correspondante à un edge
    set_lettre get_lettres(const vertex&a , const vertex&b)const;

    /// Renvoie la lettre correspondante à un edge
    set_lettre get_lettres(iterator pa , iterator pb) const;

    /// Lie un edge à une lettre
    bool lier_lettre(const vertex&a , const vertex&b,lettre l);

    /// Insertion d'un edge avec ses lettres
    void insert_edge_lettre(const vertex&a , const vertex&b,const set_lettre& sl);

    /// Insertion d'un edge avec sa lettre
    void insert_edge_lettre(const vertex&a , const vertex&b,lettre l);

    /// Insertion d'un edge avec sa lettre
    void insert_edge_lettre(iterator pa , iterator pb, lettre l);

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Écriture >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Ériture du graphe dans un .dot
    void write_dot(std::string filename,std::string name = "",bool erase = true)const;
    virtual std::string write_lettre(const lettre&l)const{return "CACA";}
    virtual std::string write(const_iterator&pv)const{return write(node(pv));}
    virtual std::string write(const vertex&v)const{return std::to_string(v);}
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< FIN ACHILLE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    virtual bool insert_vertex(const vertex &a)
    {
        if(G_.find(a)!=G_.end())
            return false;
        G_[a];
        return true;
    }

    void insert_new_vertex_inout_list(const vertex &a, const vertex_set &IN,
              const vertex_set &OUT)
    {
      typename adj_graph::iterator p = find(a);

      // return if "a" already in graph
      if (p != G_.end())
      {
            // remove the old number of OUT vertices
            num_edges_ -= p->second.second.size();
      }

      G_[a] = make_pair(IN, OUT);
      num_edges_ += OUT.size();
    }



    void insert_edge_noloop(iterator pa, iterator pb)
    {
      if (pa==pb) return;
      insert_edge(pa, pb);
    }

    void insert_edge(iterator pa, iterator pb)
    {
      vertex a = node(pa);
      vertex b = node(pb);


      unsigned int old_size = out_neighbors(pa).size();

      out_neighbors(pa).insert(b);
      in_neighbors(pb).insert(a);

      unsigned int new_size = out_neighbors(pa).size();
      if (new_size > old_size)
      {
          num_edges_++;
      }

    }

    void insert_edge_noloop(const vertex &a, const vertex &b)
    {
      if (a==b) return;
      insert_edge(a, b);
    }


    void insert_edge(const vertex &a, const vertex& b)
    {
      iterator pa = find(a);
      if (pa == G_.end())
      {
          insert_vertex(a);
          pa = find(a);
      }

      iterator pb = find(b);
      if (pb == G_.end())
      {
          insert_vertex(b);
          pb = find(b);
      }

      insert_edge( pa, pb );
    }


    void insert_edge(const edge &E)
    {
        insert_edge(E.first, E.second);
    }

    bool remove_edge(iterator pa, iterator pb)
    {
      if (pa == end() || pb == end())
        return false;

      vertex a = node(pa);
      vertex b = node(pb);

      unsigned int old_size = out_neighbors(pa).size();
      out_neighbors(pa).erase(b);
      in_neighbors(pb).erase(a);
      if (out_neighbors(pa).size() < old_size)
        num_edges_ --;
      L_.erase(std::make_pair(node(pa),node(pb))); // << Ach : retirer la lettre
      return true;
    }


    void remove_edge(const vertex &a, const vertex& b)
    {
      iterator pa = find(a);
      if (pa == end())
        return;
      iterator pb = find(b);
      if (pb == end())
        return;
      remove_edge( pa, pb );
    }

    void remove_edge(const edge &E)
    {
        remove_edge(E.first, E.second);
    }



    void remove_vertex(iterator pa)
    {
      vertex_set  out_edges = out_neighbors(pa);
      vertex_set  in_edges =  in_neighbors(pa);

      //vertex_set & out_edges = out_neighbors(pa);
      //vertex_set & in_edges =  in_neighbors(pa);

      // remove out-going edges
      for (typename vertex_set::iterator p = out_edges.begin();
                  p!=out_edges.end(); p++)
      {
          remove_edge(pa, find(*p));
      }


      // remove in-coming edges
      for (typename vertex_set::iterator p = in_edges.begin();
                  p!=in_edges.end(); p++)
      {
          remove_edge(find(*p), pa);
      }


      G_.erase(node(pa));
      erase_final(node(pa));
    }


    void remove_vertex_set(const vertex_set &V)
    {
        for (typename vertex_set::const_iterator p=V.begin(); p!=V.end(); p++)
          remove_vertex(*p);
    }


    void remove_vertex(const vertex &a)
    {
        iterator pa = find(a);
        if (pa != G_.end())
            remove_vertex( pa);

    }

  /**
        Is vertex 'a' included in graph?

        @return true, if vertex a is present; false, otherwise.
  */
   bool includes_vertex(const vertex &a) const
   {
        return  (find(a) != G_.end());
   }


   /**
        Is edge (a,b) included in graph?

        @return true, if edge is present; false, otherwise.

   */
   bool includes_edge(const vertex &a, const vertex &b) const
   {
      return (includes_vertex(a)) ?
          includes_elm(out_neighbors(a),b): false;

      //const vertex_set &out = out_neighbors(a);
      // return ( out.find(b) != out.end());

   }


  bool includes_edge(const edge& e) const
  {
    return includes_edge(e.first, e.second);
  }

    // convert to a simple edge list for exporting
    //
    /**
        Create a new representation of graph as a list
        of vertex pairs (a,b).

        @return std::vector<edge> a vector (list) of vertex pairs
    */
    std::vector<edge> edge_list() const;

/*
    graph unions
*/

    tGraph & plus_eq(const tGraph &B)
    {

        for (const_iterator p=B.begin(); p != B.end(); p++)
        {
            const vertex &this_node = node(p);
            insert_vertex(this_node);
            const vertex_set &out = out_neighbors(p);
            for (typename vertex_set::const_iterator q= out.begin(); q != out.end(); q++)
            {
                insert_edge(this_node, *q);
            }
        }
        return *this;
    }

    tGraph intersect(const tGraph &B) const
    {
        tGraph G;
        for (const_iterator p=begin(); p != end(); p++)
        {
            const vertex &this_node = node(p);
            if (B.includes_vertex(this_node))
                 G.insert_vertex(this_node);
            {
               const vertex_set &out = out_neighbors(p);
               for (typename vertex_set::const_iterator q= out.begin();
                          q != out.end(); q++)
               {
                   if (B.includes_edge(this_node, *q))
                    G.insert_edge(this_node, *q);
               }
            }
          }
          return G;
    }

    tGraph operator*(const tGraph &B) const
    {
      return intersect(B);
    }


    tGraph minus(const tGraph &B) const
    {
        tGraph G;
        for (const_iterator p=begin(); p != end(); p++)
        {
            const vertex &this_vertex = node(p);
            if (isolated(p))
            {
              if (! B.isolated(this_vertex))
                  G.insert_vertex(this_vertex);\
            }
            else
            {
               const vertex_set &out = out_neighbors(p);
               for (typename vertex_set::const_iterator q= out.begin();
                        q != out.end(); q++)
               {
                   if (!B.includes_edge(this_vertex, *q))
                    G.insert_edge(this_vertex, *q);
               }
            }
          }
          return G;
      }

    tGraph operator-(const tGraph &B) const
    {
      return minus(B);
    }


    tGraph plus(const tGraph &B) const
    {
        tGraph U(*this);

        U.plus_eq(B);
        return U;
    }

    tGraph operator+(const tGraph &B) const
    {
      return plus(B);
    }

    tGraph & operator+=(const tGraph &B)
    {
      return plus_eq(B);
    }



#if 0
/*
   union of two graphs
*/
    tGraph Union(const tGraph &B) const
    {
        tGraph U(B);
        typedef std::vector<edge> VE;

        VE b = B.edge_list();
        for (typename VE::cosnt_iterator e = b.begin(); e != b.end(); e++)
        {
            U.insert_edge(*e);
        }
        return U;
    }

/*
   intersection of two graphs
*/
    tGraph intersect(const tGraph &B) const
    {
        tGraph I;
        typedef std::vector<tGraph::edge> VE;

        VE b = B.edge_list();
        for (typename VE::const_iterator e = b.begin(); e != b.end(); e++)
        {
          if (includes_edge(*e))
          {
              I.insert_edge(*e);
          }
        }

        for (typename tGraph::const_iterator p = B.begin(); p!=B.end();p++)
        {
          if (includes_vertex( node(p) ))
                  I.insert_vertex( node(p) );
        }
        return I;
    }

#endif




/**
    @param A vertex set of nodes (subset of G)
    @return a new subgraph containing all nodes of A
*/
    tGraph subgraph(const vertex_set &A) const
    {
        tGraph G;

        for (typename vertex_set::const_iterator p = A.begin(); p!=A.end(); p++)
        {
            const_iterator t = find(*p);
            if (t != end())
            {
              vertex_set new_in =  (A * in_neighbors(t));
              vertex_set new_out = (A * out_neighbors(t));

              G.insert_new_vertex_inout_list(*p, new_in, new_out);
            }
        }
        return G;
    }


    unsigned int subgraph_size(const vertex_set &A) const
    {
        unsigned int num_edges = 0;
        for (typename vertex_set::const_iterator p = A.begin(); p!=A.end(); p++)
        {
            const_iterator pG = find(*p);
            if (pG != this->end())
            {
              num_edges +=  intersection_size(A, out_neighbors(pG) );
            }
        }
        return num_edges;
    }


    // we don't need to divide by two since  we are only
    // counting out-edges in subgraph_size()
    //
    double subgraph_sparsity(const vertex_set &A) const
    {
       double N  = A.size();

       return (A.size() ==1 ? 0.0 : subgraph_size(A)/(N * (N-1)));
    }

    static const vertex &node(const_iterator p)
    {
        return p->first;
    }

    static const vertex &node(iterator p)
    {
        return p->first;
    }

    static const vertex &node( const_vertex_iterator p)
    {
      return *p;
    }


static const vertex_set & in_neighbors(const_iterator p)
    { return (p->second).first; }

static    vertex_set & in_neighbors(iterator p)
      { return (p->second).first; }

static const_vertex_iterator in_begin(const_iterator p)
{
    return in_neighbors(p).begin();
}


static const_vertex_iterator in_end(const_iterator p)
{
    return in_neighbors(p).end();
}


static const vertex_set& out_neighbors(const_iterator p)
      { return (p->second).second; }

static const_vertex_iterator out_begin(const_iterator p)
{
    return out_neighbors(p).begin();
}


static const_vertex_iterator out_end(const_iterator p)
{
    return out_neighbors(p).end();
}


static     vertex_set& out_neighbors(iterator p)
      { return (p->second).second; }

static vertex_iterator out_begin(iterator p)
{
    return out_neighbors(p).begin();
}


static  unsigned int num_edges(const_iterator p)
  {
     return out_neighbors(p).size();
  }

inline static  unsigned int num_edges(iterator p)
  {
     return out_neighbors(p).size();
  }

/**
    @param p tGraph::const_iterator
    @return number of edges going out (out-degree) at node pointed to by p.
*/
inline static  unsigned int out_degree(const_iterator p)
  {
    return (p->second).second.size();
  }

inline static  unsigned int out_degree(iterator p)
  {
    return (p->second).second.size();
  }

/**
    @param p tGraph::const_iterator
    @return number of edges going out (out-degree) at node pointed to by p.
*/
inline static  unsigned int in_degree(const_iterator p)
  {
    return (p->second).first.size();
  }

inline static  unsigned int in_degree(iterator p)
  {
    return (p->second).first.size();
  }

inline static  unsigned int degree(const_iterator p)
  {
     return in_neighbors(p).size() + out_neighbors(p).size();
  }

inline static  unsigned int degree(iterator p)
  {
     return in_neighbors(p).size + out_neighbors(p).size();
  }


inline static bool isolated(const_iterator p)
{
    return (in_degree(p) == 0 && out_degree(p) == 0 );
}

static bool isolated(iterator p)
{
    return (in_degree(p) == 0 && out_degree(p) == 0 );
}



/**
      abosrb(a,b):   'a' absorbs 'b', b gets removed from graph
*/
void absorb(iterator pa, iterator pb)
{

    //std::cerr << "Graph::absorb(" << node(pa) << ", " << node(pb) << ")\n";

    if (pa == pb)
      return;


    // first remove edge (a,b) to avoid self-loops
    remove_edge(pa, pb);

    // chnage edges (b,i) to a(i,j)
    //
    {
    vertex_set b_out = out_neighbors(pb);
    for (typename vertex_set::iterator p = b_out.begin();
              p!=b_out.end(); p++)
    {
      iterator pi = find(*p);
      remove_edge(pb, pi);
      //std::cerr<<"\t remove_edge("<<node(pb)<< ", " << node(pi) <<")\n";
      insert_edge(pa, pi);
      //std::cerr<<"\t insert_edge("<<node(pa)<< ", " << node(pi) <<")\n";
    }
    }

    // change edges (i,b) to (i,a)
    {
    vertex_set b_in = in_neighbors(pb);
    for (typename vertex_set::iterator p = b_in.begin();
              p!=b_in.end(); p++)
    {
      iterator pi = find(*p);
      remove_edge(pi, pb);
      //std::cerr<<"\t remove_edge("<<node(pi)<< ", " << node(pb) <<")\n";
      insert_edge(pi, pa);
      //std::cerr<<"\t insert_edge("<<node(pi)<< ", " << node(pa) <<")\n";
    }
    }


    //std::cout<<"\t about to remove vertex: "<< node(pb) << "\n";

    remove_vertex(pb);
    //G_.erase( node(pb) );

    //std::cout<<"\t removed_vertex.\n";


}

/**
    c smart_abosrb(a,b):   'a' absorbs 'b', or b aborbs a, depending on
            whichever causes the least amount of graph updates
*/
iterator smart_absorb(iterator pa, iterator pb)
{
    if (degree(pa) >= degree(pb))
    {
      absorb(pa, pb);
      return pb;
    }
    else
    {
      absorb(pb, pa);
      return pb;
    }
}


vertex smart_absorb(vertex a, vertex b)
{
    iterator pa = find(a);
    if (pa == end())
    {
        return b;
    }

    iterator pb = find(b);
    if (pb == end())
    {
        return a;
    }

    iterator pc = smart_absorb(pa, pb);
    return node(pc);
}


void absorb(vertex a, vertex b)
{
    if (a == b)
      return ;

   iterator pa = find(a);
   if (pa == end())
      return;

   iterator pb = find(b);
   if (pb == end())
      return;

    absorb( pa, pb );
}


};
#include "NG_final.tpp"
#include "NG_quotient.tpp"
#include "NG_kRI.tpp"
#include "NG_accepte.tpp"
#include "NG_fusionAndPTA.tpp"
#include "NG_lettre.tpp"
#include "NG_show_utility.tpp"
#include "NG_RPNI.tpp"
#include "NG_random.tpp"
}
#endif
// NGRAPH_H_
