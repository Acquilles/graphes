template<typename T,typename C>
bool tGraph<T,C>::is_lettre_in(const typename tGraph<T,C>::vertex& a, const typename tGraph<T,C>::vertex&b,const typename tGraph<T,C>::lettre& l)const
{
    set_lettre sl = get_lettres(a,b);
    return sl.find(l) != sl.end();
}

template<typename T,typename C>
/// Renvoie la lettre correspondante à un edge
typename tGraph<T,C>::set_lettre tGraph<T,C>::get_lettres(const typename tGraph<T,C>::vertex&a , const typename tGraph<T,C>::vertex&b)const
{
  edge e = std::make_pair(a,b);
  if(L_.find(e)==L_.end())
  {
      return tGraph<T,C>::set_lettre();
  }
  return L_.at(e);
}

template<typename T,typename C>
/// Renvoie la lettre correspondante à un edge
typename tGraph<T,C>::set_lettre tGraph<T,C>::get_lettres(typename tGraph<T,C>::iterator pa , typename tGraph<T,C>::iterator pb) const
{
  return get_lettres(node(pa),node(pb));
}

template<typename T,typename C>
/// Lie un edge à une lettre
bool tGraph<T,C>::lier_lettre(const typename tGraph<T,C>::vertex&a , const typename tGraph<T,C>::vertex&b,lettre l)
{
  edge e = std::make_pair(a,b);
  if(L_.find(e)==L_.end())
    L_[e] = tGraph<T,C>::set_lettre();
  L_[e].insert(l);
  return true;
}

template<typename T,typename C>
/// Insertion d'un edge avec ses lettres
void tGraph<T,C>::insert_edge_lettre(const typename tGraph<T,C>::vertex&a , const typename tGraph<T,C>::vertex&b,const typename tGraph<T,C>::set_lettre& sl)
{
  insert_edge(a,b);
  for(const lettre& l : sl)
    lier_lettre(a,b,l);
}

template<typename T,typename C>
/// Insertion d'un edge avec sa lettre
void tGraph<T,C>::insert_edge_lettre(const typename tGraph<T,C>::vertex&a , const typename tGraph<T,C>::vertex&b,typename tGraph<T,C>::lettre l)
{
  insert_edge(a,b);
  lier_lettre(a,b,l);
}

template<typename T,typename C>
/// Insertion d'un edge avec sa lettre
void tGraph<T,C>::insert_edge_lettre(typename tGraph<T,C>::iterator pa , typename tGraph<T,C>::iterator pb, typename tGraph<T,C>::lettre l)
{
  insert_edge(node(pa),node(pb));
  lier_lettre(node(pa),node(pb),l);
}
