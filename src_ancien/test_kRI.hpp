#ifndef test_KRI_H_
#define test_KRI_H_


#include "NGraph.hpp"
#include "utiles.h"

typedef unsigned int vertex;
typedef char lettre;
typedef typename NGraph::tGraph<vertex,lettre> Graph;

// Compare les k-RI obtenus pour différentes valeurs de k
void diff_kri_k(const Graph &MCA, size_t mink, size_t maxk, std::string filename)
{
	MCA.write_dot(filename,"MCA");
	MCA.get_PTA().write_dot(filename,"PTA",false);
	double debut,fin;
	for (size_t k = mink; k <= maxk; k++)
	{
		std::string msg = "k"+ std::to_string(k);
		debut = chrono();
		MCA.get_kRI(k).write_dot(filename,msg,false);
		fin = chrono();
		std::cout << "KRI (" << k << ") : " << fin - debut << std::endl;;
	}
}


bool test_kRI(std::string filename, size_t k = 1, size_t kmin = 0, size_t kmax = 3)
{
	std::cerr << "-------- TEST kRI --------" << std::endl;
	Graph::echantillon positif = get_echantillon(filename);
	Graph MCA(positif);
	diff_kri_k(MCA,1,4,"kRI_comparaison.dot");
	Graph kRI = MCA.get_kRI(k,"kRI_evolution.dot");
	std::cerr << "-------- FIN  kRI --------" << std::endl;
	return kRI.complet(positif);
}

#endif
