template<typename T,typename C>
void tGraph<T,C>::get_RPNI_naive(const echantillon& negatif,std::string filename)const
{
    typename NGraph::tGraph<T,C>::tGraph R(*this);
    R.to_RPNI_naive(negatif,filename);
    return R;
}

template<typename T,typename C>
void tGraph<T,C>::to_RPNI_naive(const typename NGraph::tGraph<T,C>::echantillon& negatif,std::string filename)
{
    // Vecteur des états :
    std::vector<T> etats;
    for(const_iterator pv = begin();pv!=end();pv++)
    {
        etats.push_back(node(pv));
    }

    // Essayer chaque possibilité de fusion
    for (size_t i = 0; i < etats.size(); i++)
    {
        for (size_t j = 0; j < i; j++)
        {
            if(includes_vertex(etats[i]) and includes_vertex(etats[j]))
            {
                tGraph A(*this);

                // Fusion de l'état i dans l'état j, important dans ce sens
                A.fusion_deterministe(etats[j],etats[i]);
                if(A.correct(negatif))
                {
                    clone(A);
                    if(filename != "")
                    {
                        std::ostringstream nom;
                        nom <<'{'<<etats[j] << ","<< etats[i] << "}";
                        write_dot(filename,nom.str(),false);
                    }
                }
            }
        }
    }
}
