template<typename T,typename C>
tGraph<T,C> tGraph<T,C>::get_quotient(const vertex_set_set& pi) const
{
    tGraph Q(*this);
    Q.to_quotient(pi);
    return Q;
}

template<typename T,typename C>
bool tGraph<T,C>::to_quotient(const vertex_set_set& pi)
{
    bool change = false;
    for(const vertex_set& vs : pi)
    {
        vertex first = *vs.begin();
        for(const vertex& v : vs)
        {
                if(v!=first)
                {
                    fusion_lettre(find(first),find(v));
                    change = true;
                }
        }
    }
    return change;
}


template<typename T,typename C>
tGraph<T,C> tGraph<T,C>::get_quotient(const vertex&a,const vertex&b) const
{
    tGraph Q(*this);
    Q.to_quotient(a,b);
    return Q;
}

template<typename T,typename C>
bool tGraph<T,C>::to_quotient(const vertex&a,const vertex&b)
{
    if(find(a)!=end() and find(b) != end())
        fusion_lettre(find(a),find(b));
        return true;
    return false;
}
