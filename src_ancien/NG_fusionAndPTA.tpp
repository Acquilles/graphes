template<typename T,typename C>
typename tGraph<T,C>::vertex_set_set tGraph<T,C>::get_bloc_to_determinize()const
{
    /// Éiminer les cas d'indéterminisme
    vertex_set_set to_merge;
    for (const_iterator p = begin(); p != end(); p++)
    {
        /// - - On réunit les ensembles des vertex descendants associés à leur lettre dans lettre_to_set
        vertex_set p_in = out_neighbors(p);
        std::map<lettre,std::set<vertex>> lettre_to_set;
        for(const vertex& b : p_in)
        {
            set_lettre sl = get_lettres(node(p),b);
            for(const lettre & l : sl)
            {
                if(lettre_to_set.find(l) == lettre_to_set.end())
                {
                    lettre_to_set[l] = std::set<vertex>();
                }
                lettre_to_set[l].insert(b);
            }
        }

        for(const std::pair<lettre,vertex_set>& l_set : lettre_to_set)
        {
            if(l_set.second.size()>1)
            {
                to_merge.insert(l_set.second);
            }
        }
    }
    return to_merge;
}

template<typename T,typename C>
bool tGraph<T,C>::step_determinize()
{
    return to_quotient(get_bloc_to_determinize());
}


/// Renvoie le PTA du Graphe
template<typename T,typename C>
tGraph<T,C> tGraph<T,C>::get_PTA() const
{
    tGraph<T,C> PTA(*this);
    PTA.to_PTA();
    return PTA;
}

/// Éxécute le PTA sur le graphe
template<typename T,typename C>
void tGraph<T,C>::to_PTA()
{
    recursive_PTA(begin());
}

/// Éxécute le liens des similaires et récursive sur ses nouveaux descendants
template<typename T,typename C>
void tGraph<T,C>::recursive_PTA(iterator pa)
{
    fusion_similaires(pa);
    vertex_set a_out = out_neighbors(pa);
    for (typename vertex_set::iterator pb = a_out.begin(); pb!=a_out.end(); pb++)
    {
        if(find(*pb)!=pa)
            recursive_PTA(find(*pb));
    }
}

/// Fusionne tous les états descendants ayant la même lettre sur leur edge
template<typename T,typename C>
bool tGraph<T,C>::fusion_similaires(const vertex&a)
{
    // Test de la présence du vertex
    iterator pa = find(a);
    if (pa == G_.end())
    {
        return false;
    }

    return fusion_similaires(pa);
}

/// Fusionne tous les états descendants ayant la même lettre sur leur edge
template<typename T,typename C>
bool tGraph<T,C>::fusion_similaires(iterator pa)
{
    std::map<lettre,iterator> lettre_lien;
    vertex_set a_out = out_neighbors(pa);

    /// - Pour chaque descendant :
    for (typename vertex_set::iterator pb = a_out.begin(); pb!=a_out.end(); pb++)
    {
        /// - - Si on a pas déjà rencontré la lettre, on fait la fusion
        set_lettre ls = get_lettres(node(pa),node(pb));
        for(const C& l: ls)
        {
            if(lettre_lien.find(l) == lettre_lien.end())
            {
                lettre_lien[l] = find(*pb);
            }
            else
            {
                fusion_out_lettre(lettre_lien[l],find(*pb));
            }
        }

    }
    return true;
}

/// Insertion en pa des parent de pb et des lettres associées à leurs edges
template<typename T,typename C>
void tGraph<T,C>::fusion_in_lettre(iterator pa, iterator pb, bool removepb )
{
    vertex_set b_in = in_neighbors(pb);
    for(typename vertex_set::iterator pc = b_in.begin(); pc!=b_in.end(); pc++)
    {
        insert_edge_lettre(node(pc),node(pa),get_lettres(find(*pc),pb));
    }
    if(is_final(node(pb)))
        set_final(node(pa));
    if(removepb)
        remove_vertex(pb);
}

/// Insertion en pa des descendants de pb et des lettres associées à leurs edges
template<typename T,typename C>
void tGraph<T,C>::fusion_out_lettre(iterator pa, iterator pb, bool removepb)
{
    vertex_set b_out = out_neighbors(pb);
    for(typename vertex_set::iterator pc = b_out.begin(); pc!=b_out.end(); pc++)
    {
        insert_edge_lettre(node(pa),node(pc),get_lettres(pb,find(*pc)));
    }
    if(is_final(node(pb)))
        set_final(node(pa));
    if(removepb)
        remove_vertex(pb);
}

/// Insertion en pa des descendants et parents de pb et des lettres associées à leurs edges
template<typename T,typename C>
void tGraph<T,C>::fusion_lettre(iterator pa, iterator pb, bool removepb)
{
    fusion_in_lettre(pa,pb,false);
    fusion_out_lettre(pa,pb,true);
}
