# Résumé
J'ai implémenté en C++ un algorithme d'inférence grammaticale existant, et tenté d'y apporter une amélioration, pour des résultats mitigés.

## Contexte
L'inférence grammaticale a pour but de reconstruire un automate probabiliste succeptible d'avoir écrit une collection de mots (suites de symboles) donnée. L'algorithme étudié, ALERGIA, crée d'abord des états en fonction des symboles rencontrés, puis les fusionne au fur et à mesure, comme décrit en ([1](#)).

## Le projet
J'ai repris une classe de graphe déjà écrite ([2](#)) puis l'ai changée pour l'adapter à la représentation d'automates finis probabilistes, capables de manipuler des symboles et des probabilités de transition. J'ai ensuite implémenté l'algorithme ALERGIA, et l'ai testé en essayant de reconstruire des automates à partir de séquences écrites par d'autres automates formés aléatoirement. L'inférence a fonctionné pour des petits automates (2 états) mais pas pour des plus gros. J'ai apporté une amélioration qui a mieux fonctionné pour certains cas.


[1](#). Carrasco, Rafael & Oncina, Jose. (2002). Learning Stochastic Regular Grammars by Means of a State Merging Method. 10.1007/3-540-58473-0_144.

[2](#). NGraph, a simple (Network) Graph library in C++, https://math.nist.gov/~RPozo/ngraph/


# Dev note
* Le dossier src_ancien contient le code avec les templates, que je n'utilise plus car trop lourd
