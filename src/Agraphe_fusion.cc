#include "Agraphe.hpp"
void Agraphe::determinize()
{
    std::set<vertex> deja_fait;
    recursive_determinize(begin(),deja_fait);
}
void Agraphe::recursive_determinize(iterator pa, std::set<vertex>&deja_fait)
{
    /// - Si le vertex est déjà déterminisé, on n'y repasse pas
    if(deja_fait.find(node(pa)) != deja_fait.end())
        return;

    /// - On fusionne les vertex sortants ayant la même lettre
    fusion_similaires(pa);
    deja_fait.insert(node(pa));

    /// - On fait la même chose sur les descendants
    vertex_set a_out = out_neighbors(pa);
    for (typename vertex_set::iterator pb = a_out.begin(); pb!=a_out.end(); pb++)
    {
        /// - Inutile de le faire sur le même, car fusion_similaires gère ça
        if(find(*pb)!=pa)
            recursive_determinize(find(*pb),deja_fait);
    }
}

/// Fusionne tous les états descendants ayant la même lettre sur leur edge
bool Agraphe::fusion_similaires(iterator pa)
{
    // Indique s'il y a eu une fusion
    bool fusion = false;
    // std::cerr << "fusion_similaires(" << node(pa) <<") : ";

    // Associe chaque lettre au descendant correspondant
    std::map<lettre,iterator> lettre_lien;
    vertex_set a_out = out_neighbors(pa);

    /// - Pour chaque descendant :
    for (vertex_set::iterator pb = a_out.begin(); pb!=a_out.end(); pb++)
    {
        // std::cerr << *pb << ": ";
        /// - - Pour chaque lettre :
        set_lettre ls = get_lettres(node(pa),*pb);
        for(const lettre& l: ls)
        {
            // std::cerr << l << "... ";

            /// - - - Si c'est permière fois qu'on rencontre la lettre, on ne fait rien
            if(lettre_lien.find(l) == lettre_lien.end())
            {
                lettre_lien[l] = find(*pb);
            }

            /// - - - Sinon, on fait la fusion,
            /// - - - En recommençant la fonction si on s'est fusionné nous même
            else
            {
                // std::cerr << "fusion("<<node(lettre_lien[l])<<','<<*pb<<") ";
                // fusion_vertex(node(pa),node(pb));
                fusion_lettre(lettre_lien[l],find(*pb));

                // Cas ou on a fusionné l'autre en nous
                if(lettre_lien[l] == pa)
                {
                    // std::cerr << std::endl << "Stay ("<<node(pa)<<") : ";
                    return fusion_similaires(pa);
                }

                // Cas ou on a fusionné nous en l'autre
                if(*pb == node(pa))
                {
                    // std::cerr << std::endl << "Move ("<<node(lettre_lien[l])<<") : ";
                    return fusion_similaires(lettre_lien[l]);
                }
                fusion = true;
            }
        }
    }
    // std::cerr << std::endl;
    return fusion;
}

Agraphe Agraphe::get_quotient(const vertex_set_set& pi) const
{
    Agraphe Q(*this);
    Q.to_quotient(pi);
    return Q;
}


bool Agraphe::to_quotient(const vertex_set_set& pi)
{
    bool change = false;
    for(const vertex_set& vs : pi)
    {
        vertex first = *vs.begin();
        for(const vertex& v : vs)
        {
                if(v!=first)
                {
                    fusion_lettre(find(first),find(v));
                    change = true;
                }
        }
    }
    return change;
}



Agraphe Agraphe::get_quotient(const vertex&a,const vertex&b) const
{
    Agraphe Q(*this);
    Q.to_quotient(a,b);
    return Q;
}


bool Agraphe::to_quotient(const vertex&a,const vertex&b)
{
    if(find(a)!=end() and find(b) != end()){
        fusion_lettre(find(a),find(b));
        return true;}
    return false;
}

/// Insertion en pa des parent de pb et des lettres associées à leurs edges
void Agraphe::fusion_in_edges(iterator pa, iterator pb, bool removepb )
{
    vertex_set b_in = in_neighbors(pb);
    for(vertex_set::iterator pc = b_in.begin(); pc!=b_in.end(); pc++)
    {
        for(const lettre&l : get_lettres(find(*pc),pb))
        {
            // Dans le cas où le lien à fusionner existe déjà
            if(!is_lettre_in(node(pc),node(pa),l))
                insert_edge_lettre(node(pc),node(pa),l);
            fusion_edge(node(pc),node(pa),node(pc),node(pb),l);

        }
    }

    if(is_final(node(pb)))
        set_final(node(pa));
    if(removepb)
        remove_vertex(pb);
}

/// Insertion en pa des descendants de pb et des lettres associées à leurs edges
void Agraphe::fusion_out_edges(iterator pa, iterator pb, bool removepb )
{
    vertex_set b_out = out_neighbors(pb);
    for(vertex_set::iterator pc = b_out.begin(); pc!=b_out.end(); pc++)
    {
        for(const lettre&l : get_lettres(pb,find(*pc)))
        {
            // Dans le cas où le lien à fusionner existe déjà
            if(!is_lettre_in(node(pa),node(pc),l))
                insert_edge_lettre(node(pa),node(pc),l);
            fusion_edge(node(pa),node(pc),node(pb),node(pc),l);

        }
    }
    if(is_final(node(pb)))
        set_final(node(pa));
    if(removepb)
        remove_vertex(pb);
}

/// Fusionne les deux états dans le premier
void Agraphe::fusion_lettre(iterator pa, iterator pb, bool removepb)
{
    fusion_vertex(node(pa),node(pb));
    fusion_in_edges(pa,pb,false);
    fusion_out_edges(pa,pb,true);
}

/// Fonction de fusion de deux liens
bool Agraphe::fusion_edge(const vertex&aa,const vertex&ab,const vertex&ba,const vertex&bb,const lettre&l){return true;}
/// Fonction de fusion de deux vertex
bool Agraphe::fusion_vertex(const vertex&a,const vertex&b){return true;}
