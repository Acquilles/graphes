#include "Agralergia.hpp"

#include <cmath>
// https://people.sc.fsu.edu/~jburkardt/cpp_src/asa111/asa111.html
// https://www.gnu.org/software/gsl/doc/html/randist.html
typedef typename Agralergia::vertex vertex;

bool Agralergia::fusion_vertex(const vertex&a,const vertex&b)
{
    return Agrafreq::fusion_vertex(a,b);
}

// <<<<<<<<<<<<<<<<<<<<LOG VRAISSEMBLANCE LOCALE (LVL)>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

double Agralergia::calculer_lvl(const_iterator pa,const_iterator pb)const
{
    vertex a(node(pa)),b(node(pb));
    double LV = 0.0;
    LV = log(pval_freq(get_end(a),get_occ(a),get_end(b),get_occ(b)));

    std::set<lettre> lettres = get_lettres_out(pa,pb);
    // Tester leur similarité
    for(const lettre& l : lettres)
    {
        unsigned int fa = frequence_lettre(a,l),fb = frequence_lettre(b,l);
        LV+=log(pval_freq(fa,get_occ(a),fb,get_occ(b)));
    }
    return LV;
}

bool Agralergia::set_lvl(const vertex&a,const vertex&b,double lvr)
{
    if(a==b)
        return false;
    if(a>b)
        LVL_[std::make_pair(b,a)]=lvr;
    else
        LVL_[std::make_pair(a,b)]=lvr;
    return true;
}

/// Attribut à chaque sommet sa lvl pour chaque sommet (log vraissemblance locale)
void Agralergia::set_lvl_all()
{
    for(const_iterator pa = begin(); pa!=end();pa++)
    {
        for(const_iterator pb = begin(); pb!=pa;pb++)
        {
            set_lvl(node(pb),node(pa),calculer_lvl(pb,pa));
            // std::err.precision(2);
            // std::err << node(pb) << " & " << node(pa) << " : " << get_lvl(node(pb),node(pa)) << std::endl;
        }
    }
}

double Agralergia::get_lvl(const vertex&a,const vertex&b)const
{
    if(a==b)
        return 0;
    vertex aa(a),bb(b);
    if(a>b)
    {
        aa = b;
        bb =a;
    }
    assertm(LVL_.find(std::make_pair(aa,bb))!=LVL_.end(),"Error get_lvl");
    return LVL_.at(std::make_pair(aa,bb));
}



std::tuple<vertex,vertex,double> Agralergia::get_max_lvl()const
{
    vertex maxa(0),maxb(0);
    double max_lvl = -1000;
    double tmp_lvl;
    for(const_iterator pa = begin(); pa!=end();pa++)
    {
        for(const_iterator pb = begin(); pb!=pa;pb++)
        {
            tmp_lvl = calculer_lvl(pa,pb);
            if( tmp_lvl > max_lvl)
            {
                max_lvl = tmp_lvl;
                maxa= node(pa);
                maxb=node(pb);
            }
        }
    }
    if(maxa!=maxb)
        return std::make_tuple(maxb,maxa,max_lvl);;
    return std::make_tuple(0,0,INFINITY);
}

void Agralergia::print_lvl_tree()const
{
    for(const_iterator pa = begin(); pa!=end();pa++)
    {
        for(const_iterator pb = pa; pb!=end();pb++)
        {
            std::cout << node(pa) << " & " << node(pb) << " : " << get_lvl(node(pa),node(pb)) << "\n";
        }
    }
}

void Agralergia::to_alergia_max_lvl()
{
    vertex a,b;
    double score;
    while(std::isfinite(score))
    {
        std::tie(a,b,score) = get_max_lvl();
        std::ostringstream s;
        s.precision(2);
        s << a << " & " << b << " : " << score;
        std::cerr << s.str() << std::endl;
        if(std::isfinite(score))
            fusion_deterministe(a,b);
        write_dot("alergiachille.dot",s.str(),false);
    }
}

// <<<<<<<<<<<<<<<<<<<<LOG VRAISSEMBLANCE RECURSIVE (LVR)>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>;

bool Agralergia::set_lvr(const vertex&a,const vertex&b,double lvl)
{
    if(a>b)
        LVR_[std::make_pair(b,a)]=lvl;
    else
        LVR_[std::make_pair(a,b)]=lvl;
    return true;
}

void Agralergia::calculer_lvr(const vertex &a,const vertex& b)
{
    if(a==b)
        return;
    double LV = get_lvl(a,b);

    // Par défaut, pour éviter les boucles ...
    set_lvr(a,b,0);
    map_l_to_it l_to_it = get_l_to_it(a,b);
    vertex na,nb;
    for(std::pair<lettre,std::pair<iterator,iterator>> il : l_to_it)
    {
        // Si les deux sont en communs
        if(il.second.first !=end() and il.second.second !=end())
        {
            na = node(il.second.first);
            nb = node(il.second.second);
            if(!is_calc_lvr(na,nb))
                calculer_lvr(na,nb);
            LV+=get_lvr(na,nb);
        }
    }
    set_lvr(a,b,LV);
}

void Agralergia::calculer_lvr_all()
{
    for(const_iterator pa = begin(); pa!=end();pa++)
    {
        for(const_iterator pb = begin(); pb!=pa;pb++)
        {
            if(!is_calc_lvr(node(pb),node(pa)))
            {
                calculer_lvr(node(pb),node(pa));
            }
        }
    }
}

double Agralergia::get_lvr(const vertex&a,const vertex&b)const
{
    if(a==b)
        return 0;
    vertex aa(a),bb(b);
    if(a>b)
    {
        aa = b;
        bb =a;
    }
    assertm(LVR_.find(std::make_pair(aa,bb))!=LVR_.end(),"Error get_lvr");

    return LVR_.at(std::make_pair(aa,bb));
}



bool Agralergia::is_calc_lvr(const vertex&a,const vertex&b)
{
    if(a==b)
        return true;
    vertex aa(a),bb(b);
    if(a>b)
    {
        aa = b;
        bb =a;
    }
    return LVR_.find(std::make_pair(aa,bb))!=LVR_.end();
}

void Agralergia::print_lvr_tree()
{
    for(const_iterator pa = begin(); pa!=end();pa++)
    {
        for(const_iterator pb = pa; pb!=end();pb++)
        {
            std::cout << node(pb) << " & " << node(pa) << " : " << get_lvr(node(pb),node(pa)) << "\n";
        }
    }
    std::cout << std::endl;
}

/// Indique si (aa,ab) < (ba,bb) au sens du lvr corrigé
/// Celle-ci corrige juste les cas d'égalité avec le nombre de liens
// VERSION 1, avec le nombre de gars qui se barrent
/*bool Agralergia::lvr_inf(const vertex &aa,const vertex& ab,const vertex &ba,const vertex& bb)
{
    double lvr_a = get_lvr(aa,ab);
    double lvr_b = get_lvr(ba,bb);
    if(lvr_a == lvr_b)
        return get_nb_liens(aa)+get_nb_liens(ab) < get_nb_liens(ba) + get_nb_liens(bb);
    return lvr_a < lvr_b;
}*/

/// Indique un score de passsage de deux vertices
unsigned int Agralergia::score_passage(const vertex& a,const vertex&b)const
{
    return (get_occ(a)-get_end(a))+(get_occ(b)-get_end(b));
}

bool Agralergia::lvr_inf(const vertex &aa,const vertex& ab,const vertex &ba,const vertex& bb)
{
    double sa(score_lvr(aa,ab)),sb(score_lvr(ba,bb));
    if(!std::isfinite(sa) and!std::isfinite(sb))
        return get_lvl(aa,ab)<get_lvl(ba,bb);

    if(!std::isfinite(sb))
        return false;
    if(!std::isfinite(sa))
        return true;
    return sa < sb;
}

double Agralergia::score_lvr(const vertex &a,const vertex& b)const
{
    unsigned int oa(get_occ(a)),ea(get_end(a));
    unsigned int ob(get_occ(b)),eb(get_end(b));
    if(oa-ea==0)
        return -INFINITY;
    if(ob-eb == 0)
        return -INFINITY;
    return get_lvr(a,b);

}
std::tuple<vertex,vertex,double> Agralergia::get_max_lvr()
{
    ///  Corriger
    /// - -1000 -> -inf
    /// - le faire directement dans calculer_lvr_all
    vertex maxa(0),maxb(0);
    for(const_iterator pa = begin(); pa!=end();pa++)
    {
        for(const_iterator pb = begin(); pb!=pa;pb++)
        {
            if(lvr_inf(maxa,maxb,node(pa),node(pb)) or maxa == maxb)
            {
                maxa= node(pa);
                maxb=node(pb);
            }
        }
    }


    double max_lvl = get_lvr(maxa,maxb);
    if(maxa!=maxb)
        return std::make_tuple(maxb,maxa,max_lvl);
    return std::make_tuple(0,0,INFINITY);
}


void Agralergia::to_alergia_max_lvr(std::string filedot)
{
    ///  Corriger
    /// - clear -> que ceux qui doivent être recalculés
    vertex a,b;
    double score;
    while(std::isfinite(score))
    {
        clear_lv();
        set_lvl_all();
        calculer_lvr_all();
        std::tie(a,b,score) = get_max_lvr();
        std::ostringstream s;
        s.precision(2);
        s << a << '('<<corv(a)<<')'<<" & " << b  << '('<<corv(b)<<')'<< " : " << score_lvr(a,b);
        std::cerr << "----- "<< s.str() << " -----" <<std::endl;
        if(corv(a)!=corv(b))
            print_lv_tree_tri();
        std::cerr << "----- ---------------- -----" <<std::endl<<std::endl;
        if(std::isfinite(score))
            fusion_deterministe(a,b);
        if(filedot!="")
            write_dot(filedot,s.str(),false);
    }
}

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< LES DEUX >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


void Agralergia::print_lv_tree()const
{
    for(const_iterator pa = begin(); pa!=end();pa++)
    {
        for(const_iterator pb = pa; pb!=end();pb++)
        {
            if(std::isfinite(get_lvr(node(pb),node(pa))))
            {
                std::cout << node(pa)  <<" & " << node(pb) <<" ("<<corv(node(pa))<<"&"<<corv(node(pb)) << ')';
                std::cout << " : L " << get_lvl(node(pb),node(pa)) << " R "<<get_lvr(node(pb),node(pa)) <<" S "<<score_lvr(node(pb),node(pa))<< "\n";
            }
        }
    }
    std::cout << std::endl;
}

void Agralergia::clear_lv()
{
    LVR_.clear();
    LVL_.clear();
}
typedef std::pair<unsigned int, unsigned int> helas;
typedef std::pair<helas, double> helice;

bool comp_lvr(const helice&a,const helice&b)
{
    return a.second < b.second;
}

std::vector<helice> sort_map(const std::map<helas,double>&m)
{
    std::vector<helice> v;
    for(const helice &h : m)
    {
        if(std::isfinite(h.second))
            v.push_back(h);
    }
    std::sort(v.begin(),v.end(),comp_lvr);
    return v;
}

void Agralergia::print_lv_tree_tri()const
{
    for(const helice & h : sort_map(LVR_))
    {
        std::cout << h.first.first  <<" & " <<  h.first.second<<" ("<<corv( h.first.first)<<"&"<<corv( h.first.second) << ')';
        std::cout << " : L " << get_lvl(h.first.first,h.first.second) << " R "<<h.second <<" S "<<score_lvr( h.first.second,h.first.first)<< "\n";
    }
}
// <<<<<<<<<<<<<<<<<< CORRESPONDANCE AVEC PROB >>>>>>>>>>>>>>>>>>>>>>

void Agralergia::set_corr(const Agraphe&agg)
{
    corr = get_corr(agg);
}

Agralergia::vertex Agralergia::corv(const vertex&v)const
{
    if(corr.find(v)!=corr.end())
        return corr.at(v);
    return 421;
}
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< AFFICHAGE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/// Ajout de l'affichage des occurrences du vertex
// std::string Agralergia::show_vertex(const vertex&v)const
// {
//     std::ostringstream s;
//     s << Agraphe::show_vertex(v);
//     s << "\n" << get_max_lvl_vertex(v) << " : " << get_max_lvl(v);
//     return s.str();
// }
