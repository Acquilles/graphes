#include "Agraprob.hpp"
#include "Agrafreq.hpp"
#include "Agralergia.hpp"


void test_carrasco()
{
    //https://grfia.dlsi.ua.es/repositori/grfia/pubs/57/icgi1994.pdf
    // page 151

    std::ifstream frw("randwords.txt");
    Agrafreq af(frw);
    af.write_dot("test_carrasco.dot","MCA",true);
    af.determinize();
    af.write_dot("test_carrasco.dot","PTA",false);
    af.fusion_lettre(af.find(0),af.find(4));
    af.determinize();
    af.write_dot("test_carrasco.dot","fu 0 4",false);
    af.fusion_lettre(af.find(0),af.find(10));
    af.determinize();
    af.write_dot("test_carrasco.dot","fu 0 10",false);
    af.fusion_lettre(af.find(1),af.find(2));
    af.determinize();
    af.write_dot("test_carrasco.dot","fu 1 2",false);
    af.get_agraprob().write_dot("test_carrasco.dot","PROBA",false);
}

void test_basic()
{
    // Former le graphe
    Agrafreq a;
    a.insert_edge_lettre(0,1,1);
    a.insert_edge_lettre(0,2,1);
    a.insert_edge_lettre(2,3,3);
    a.insert_edge_lettre(1,4,4);
    a.insert_edge_lettre(0,5,0);
    a.insert_edge_lettre(0,5,1);
    a.insert_edge_lettre(0,5,2);
    a.insert_edge_lettre(1,4,2);
    a.increase_pass(0,1,1,4);
    a.increase_pass(0,2,1,2);

    // Tester la fusion
    a.write_dot("test_basic.dot","A");
    a.fusion_lettre(a.find(1),a.find(2));
    a.write_dot("test_basic.dot","F",false);
    a.fusion_lettre(a.find(0),a.find(1));
    a.write_dot("test_basic.dot","F",false);
    a.determinize();
    a.write_dot("test_basic.dot","D",false);
}

void test_random()
{
    // Former l'échantillon random
    std::ifstream fg("random.txt");
    Agraprob a(fg);
    fg.close();
    a.write_dot("test_random.dot","Probas originales",true);
    Agraphe::echantillon_mult ech;
    for (size_t i = 0; i < 100; i++)
        ech.push_back(a.random_mot());

    // Former l'agraphe
    Agrafreq af(ech);
    af.write_dot("test_random.dot","MCA",false);
    af.determinize();
    af.write_dot("test_random.dot","PTA",false);
    af.to_ALERGIA(0.2);
    af.write_dot("test_random.dot","ALERGIA",false);
    af.get_agraprob().write_dot("test_random.dot","Proba obtenues",false);
}

void test_lvl_carrasco()
{
    // Former l'échantillon random
    std::ifstream fg("random.txt");
    Agraprob a(fg);
    fg.close();
    a.write_dot("alergiachille.dot","Probas originales",true);
    Agraphe::echantillon_mult ech;
    for (size_t i = 0; i < 20; i++)
        ech.push_back(a.random_mot());

    // Former l'agraphe
    Agralergia af(ech);
    // af.write_dot("test_random.dot","MCA",false);
    af.determinize();

    af.write_dot("alergiachille.dot","PTA",false);
    af.to_alergia_max_lvl();
    af.write_dot("alergiachille.dot","alergia",false);

    af.get_agraprob().write_dot("alergiachille.dot","Proba obtenues",false);

}
void test_deter()
{
    Agrafreq a;
    a.insert_edge_lettre(0,1,0);
    a.insert_edge_lettre(0,2,1);
    a.insert_edge_lettre(1,3,0);
    a.insert_edge_lettre(1,4,0);
    a.insert_edge_lettre(1,5,1);
    a.insert_edge_lettre(2,6,1);
    a.insert_edge_lettre(2,2,0);
    a.insert_edge_lettre(2,0,1);

    a.write_dot("test_deter.dot","deb",true);
    a.determinize();
    a.write_dot("test_deter.dot","fu2",false);

}
