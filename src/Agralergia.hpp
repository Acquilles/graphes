#ifndef _AGRALERGIA_H_
#define _AGRALERGIA_H_
#include "Agrafreq.hpp"
#include"utiles.hpp"
class Agralergia : public Agrafreq
{
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< TYPEDEF >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

public:
    typedef std::map<std::pair<vertex,vertex>,double> dic_vraissemblance;

public:
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< CONSTRUCTEURS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    Agralergia():Agrafreq(){}
    Agralergia(std::istream&is):Agrafreq(is){}
    Agralergia(const echantillon_mult& positif):Agrafreq(positif){}

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ATTRIBUTS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
private:
    /// Log vraissemblance récursive
    dic_vraissemblance LVR_;
    /// Log vraissemblance locale
    dic_vraissemblance LVL_;
    /// Le nombre de descendants du vertex
    // pour plus tard
    /// correspondance avec le graphe prob
    std::map<vertex,vertex> corr;
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Utilitaire >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
public:
    /// Ajout pour indiquer le changement dans les LVL_
    bool fusion_vertex(const vertex&a,const vertex&b);

    // <<<<<<<<<<<<<<<<<<<<LOG VRAISSEMBLANCE LOCALE (LVL)>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>;
public:
    /// Calcule la lvl de deux sommets
    double calculer_lvl(const_iterator pa,const_iterator pb)const;

    /// Stocke la lvl de deux sommets dans l'arbre
    bool set_lvl(const vertex&a,const vertex&b,double lvl);
    /// Calcule et stocke toutes les lvl
    void set_lvl_all();


    /// Renvoie la lvl calculée de deux sommets
    double get_lvl(const vertex&a,const vertex&b)const;
    /// Renvoie les sommets qui ont la lvl maximale
    std::tuple<vertex,vertex,double> get_max_lvl()const;
    /// Afiche toutes les lvl calculées
    void print_lvl_tree()const;

    /// Convertit avec l'algo ALERGIA avec la lvl seulement
    void to_alergia_max_lvl();


// <<<<<<<<<<<<<<<<<<<<LOG VRAISSEMBLANCE RÉURISIVE (LVR)>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>;
    /// Stocke la lvr de deux sommets
    bool set_lvr(const vertex&a,const vertex&b,double lvr);
    /// Calcule et stocke la lvr de deux sommets
    void calculer_lvr(const vertex &a,const vertex& b);
    /// Calcule et stocke demi-récursivement toutes les lvr
    void calculer_lvr_all();

    /// Renvoie la lvr de deux sommets
    double get_lvr(const vertex&a,const vertex&b)const;
    /// Indique si une lvr est déjà calculée
    bool is_calc_lvr(const vertex&a,const vertex&b);
    /// Affiche l'arbre des lvr stockées
    void print_lvr_tree();

    /// Comparaison entre la lvr d'un sommet et de l'autre
    /// Sert à héirarchiser les fusions dans get_max_lvr
    bool lvr_inf(const vertex &aa,const vertex& ab,const vertex &ba,const vertex& bb);
    double score_lvr(const vertex &a,const vertex& b)const;
    /// Renvoie les sommets avec le lvr maximal au sens de la fonction lvr_inf
    std::tuple<vertex,vertex,double> get_max_lvr();
    /// Convertit avec l'algo ALERGIA avec la lvr
    void to_alergia_max_lvr(std::string filedot = "");

// <<<<<<<<<<<<<<<<<< LOG V TOTAL >>>>>>>>>>>>>>>>>>>>>>
    /// Affiche les deux LV
    void print_lv_tree()const;
    /// Affiche les deux LV
    void print_lv_tree_tri()const;
    /// Nettoie les deux LV
    void clear_lv();

// <<<<<<<<<<<<<<<<<< CORRESPONDANCE AVEC PROB >>>>>>>>>>>>>>>>>>>>>>
    /// Stocke la correspondance de chaque état avec celui de l'agraprob
    void set_corr(const Agraphe&agg);
    /// Renvoie la correspondance avec un état de l'agraprob
    vertex corv(const vertex&v)const;

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< UTILES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Indique un score de passsage de deux vertices
    unsigned int score_passage(const vertex& a,const vertex&b)const;



};
#endif
