#ifndef ACHILLE_GRAPH_H
#define ACHILLE_GRAPH_H

#include <iostream>
#include <set>
#include <list>
#include <map>
#include <utility>
#include <iterator>
#include <vector>
#include <string>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <tuple>

class Agraphe
{
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< TYPEDEF >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
public:
    typedef unsigned int lettre; // <<<< Ach
    typedef std::vector<lettre> mot;  // <<<< Ach
    typedef std::set<mot> echantillon;       // <<<< Ach
    typedef std::vector<mot> echantillon_mult;  // <<<< Ach
    typedef std::set<lettre> set_lettre;  // <<<< Ach

    typedef unsigned int vertex;
    typedef std::tuple<vertex,vertex> edge;

    typedef std::set<vertex> vertex_set;

    typedef std::set<vertex_set> vertex_set_set;  // <<<< Ach
    typedef typename std::vector<vertex> chemin; // <<<< Ach

    typedef std::set<edge> edge_set;
    typedef std::pair<vertex_set, vertex_set> in_out_edge_sets;

    typedef typename edge_set::iterator edge_iterator;
    typedef typename edge_set::const_iterator const_edge_iterator;

    typedef typename vertex_set::iterator vertex_iterator;
    typedef typename vertex_set::const_iterator const_vertex_iterator;

    typedef typename vertex_set_set::iterator vertex_set_iterator;  // <<<< Ach
    typedef typename vertex_set_set::const_iterator const_vertex_set_iterator;  // <<<< Ach


    typedef typename vertex_set::iterator vertex_neighbor_iterator;
    typedef typename vertex_set::const_iterator vertex_neighbor_const_iterator;

    typedef std::map<edge,set_lettre> lien_lettres;

    typedef std::map<vertex, in_out_edge_sets>  adj_graph;
    typedef typename adj_graph::iterator iterator;
    typedef typename adj_graph::const_iterator const_iterator;
    typedef std::map<lettre,std::pair<iterator,iterator>> map_l_to_it;

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ATTIRBUTS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
private:
    adj_graph G_;
    lien_lettres L_;
    vertex_set F_;
    unsigned int num_edges_;


// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// <<<<<<<<<<<<<<<<<<<<<<<<<<< CONSTRUCTEURS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
public:
    Agraphe(): G_(adj_graph()), num_edges_(0){}

    Agraphe(const Agraphe &B) : G_(B.G_), L_(B.L_), F_(B.F_),num_edges_(B.num_edges_){}

    void clone(const Agraphe &B){
        G_ =B.G_; L_ =B.L_; F_ =B.F_;num_edges_ =B.num_edges_;
    }

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< BASIQUES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    unsigned int num_vertices() const { return G_.size(); }
    unsigned int num_nodes() const { return G_.size(); }
    unsigned int num_edges() const { return num_edges_; }

    iterator begin() { return G_.begin(); }
    const_iterator begin() const { return G_.begin(); }
    iterator end() { return G_.end(); }
    const_iterator end() const { return G_.end(); }

    void clear()
    {
      G_.clear();
      num_edges_ = 0;
    }
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< VOISINS   >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    vertex_neighbor_iterator out_neighbors_begin(const vertex &a)
    {
      return out_neighbors(a).begin();
    }

    vertex_neighbor_const_iterator out_neighbors_begin(const vertex &a) const
    {
      return out_neighbors(a).begin();
    }

    vertex_neighbor_iterator out_neighbors_end(const vertex &a)
    {
      return out_neighbors(a).end();
    }

    vertex_neighbor_const_iterator out_neighbors_end(const vertex &a) const
    {
      return out_neighbors(a).end();
    }

    iterator find(const vertex &a)
     {
        return G_.find(a);
     }

     const_iterator find(const vertex  &a) const
     {
        return G_.find(a);
     }

    const vertex_set &in_neighbors(const vertex &a) const
              { return (find(a))->second.first;}
          vertex_set &in_neighbors(const vertex &a)
              { return (G_[a]).first; }

    const vertex_set &out_neighbors(const vertex &a)const
                {return find(a)->second.second;}
          vertex_set &out_neighbors(const vertex &a)
                {return G_[a].second; }


     unsigned int in_degree(const vertex &a) const
     {
        return in_neighbors(a).size();
     }

     unsigned int out_degree(const vertex &a) const
     {
        return out_neighbors(a).size();
     }

     unsigned int degree(const vertex &a) const
     {
        const_iterator p = find(a);
        if (p == end())
          return 0;
        else
          return degree(p);
     }


    bool isolated(const vertex &a) const
    {
        const_iterator p = find(a);
        if ( p != end() )
          return  isolated(p) ;
        else
            return false;
    }

    /// Le nombre de liens descendants du vertex
    unsigned int get_nb_liens(const vertex&v)
    {
        unsigned int nb = 0;
        for(const vertex&vb: out_neighbors(v))
            nb+=get_lettres(v,vb).size();
        return nb;
    }

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ACCEPTATION >>>>>>>>>>>>>>>>>>>>>>>>>>>>
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    bool complet(const echantillon& positif)const;

    bool correct(const echantillon& negatif)const;

    bool coherent(const echantillon& positif,const echantillon& negatif)const;
    bool accepte(const mot& acceptation)const;

    bool accepte(const_iterator pa, const mot& acceptation,size_t i)const;

    void set_final(const vertex& v);

    void set_final(iterator pv);

    void erase_final(const vertex &v);

    bool is_final(const vertex& v)const;

    /// Renvoie le premier vertex descendant de a sur une lettre l
    vertex get_vertex_lettre(const vertex& a, const lettre &l)const;
    vertex get_state(const mot&m)const;
    vertex get_state(const mot&m,const vertex&v, unsigned int i)const;
    /// Les mots de l'un dans ceux de l'autre
    std::map<vertex,vertex> get_corr(const Agraphe& agg);
    void recursive_get_corr(const Agraphe& agg,std::map<vertex,vertex>&corr,const vertex&v,const vertex&v_autre)const;
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< FUSIONS >>>>>>>>>>>>>>>>>>>>>>>>>>>>
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Renvoi le quotient de deux éléments.
    Agraphe get_quotient(const vertex&a,const vertex&b) const;

    // Quotient de deux éléments.
    bool to_quotient(const vertex&a,const vertex&b);

    /// Renvoi le quotient du graphe par pi
    Agraphe get_quotient(const vertex_set_set& pi) const;

    /// Divise le graphe par pi, c-a-d regroupe les ensembles d'états que contient pi
    bool to_quotient(const vertex_set_set& pi);

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Determinizer >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Déterminise le graphe
    void determinize();

    /// Fonction de déterminisation récursive
    void recursive_determinize(iterator pa, std::set<vertex>&deja_fait);

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< FUSION >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Fusionne les deux états et déterminise le graphe
    void fusion_deterministe(const vertex& a, const vertex& b)
    {
        fusion_lettre(find(a),find(b));
        determinize();
    }
    /// Fusionne tous les états descendants ayant la même lettre sur leur edge
    virtual bool fusion_similaires(iterator pa);

    /// Insertion en pa des parent de pb et des lettres associées à leurs edges
    virtual void fusion_in_edges(iterator pa, iterator pb, bool removepb = true);

    /// Insertion en pa des descendants de pb et des lettres associées à leurs edges
    virtual void fusion_out_edges(iterator pa, iterator pb, bool removepb = true);

    /// Insertion en pa des descendants et parents de pb et des lettres associées à leurs edges
    virtual void fusion_lettre(iterator pa, iterator pb, bool removepb = true);

    /// Fonction de fusion de deux liens
    virtual bool fusion_edge(const vertex&aa,const vertex&ab,const vertex&ba,const vertex&bb,const lettre&l);

    /// Fonction de fusion de deux vertex
    virtual bool fusion_vertex(const vertex&a,const vertex&b);
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< LETTRES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    /// Indique si la lettre est dans un vertice de l'un à l'autre
    bool is_lettre_in(const vertex& a, const vertex&b,const lettre& l)const;

    /// Renvoie la lettre correspondante à un edge
    set_lettre get_lettres(const vertex&a , const vertex&b)const;

    /// Renvoie la lettre correspondante à un edge
    set_lettre get_lettres(iterator pa , iterator pb) const;

    /// Lie un edge à une lettre
    bool lier_lettre(const vertex&a , const vertex&b,lettre l);

    /// Insertion d'un edge avec ses lettres
    virtual void insert_edge_lettre(const vertex&a , const vertex&b,const set_lettre& sl);

    /// Insertion d'un edge avec sa lettre
    virtual void insert_edge_lettre(const vertex&a , const vertex&b,const lettre& l);

    /// Renvoie l'ensemble des lettres de sortie des deux vertices
    std::set<lettre> get_lettres_out(const_iterator pa, const_iterator pb)const;

    /// Renvoie chaque lettre associée à ses itérateurs
    map_l_to_it get_l_to_it(const vertex&a,const vertex&b);

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Ériture >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Ériture du graphe dans un .dot
    void write_dot(std::string filename,std::string name = "",bool erase = true)const;
    virtual std::string show_vertex(const vertex&v)const{return std::to_string(v);}
    virtual std::string show_lien(const vertex&a,const vertex&b,const lettre&l)const{return show_lettre(l);}
    virtual std::string show_lettre(const lettre&l)const{return std::to_string(l);}
    ///
    virtual std::string show_mot(const mot&m)const;
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< INSERTIONS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    virtual bool insert_vertex(const vertex &a)
    {
        if(G_.find(a)!=G_.end())
            return false;
        G_[a];
        return true;
    }

    virtual void insert_edge(const vertex &a, const vertex& b)
    {
      iterator pa = find(a);
      if (pa == G_.end())
      {
          insert_vertex(a);
          pa = find(a);
      }

      iterator pb = find(b);
      if (pb == G_.end())
      {
          insert_vertex(b);
          pb = find(b);
      }

      insert_edge( pa, pb );
    }

    virtual void insert_edge(iterator pa, iterator pb)
    {
      vertex a = node(pa);
      vertex b = node(pb);


      unsigned int old_size = out_neighbors(pa).size();

      out_neighbors(pa).insert(b);
      in_neighbors(pb).insert(a);

      unsigned int new_size = out_neighbors(pa).size();
      if (new_size > old_size)
      {
          num_edges_++;
      }

    }


    virtual bool remove_edge(iterator pa, iterator pb)
    {
      if (pa == end() || pb == end())
        return false;

      vertex a = node(pa);
      vertex b = node(pb);

      unsigned int old_size = out_neighbors(pa).size();
      out_neighbors(pa).erase(b);
      in_neighbors(pb).erase(a);
      if (out_neighbors(pa).size() < old_size)
        num_edges_ --;
      L_.erase(std::make_pair(node(pa),node(pb))); // << Ach : retirer la lettre
      return true;
    }


    virtual void remove_edge(const vertex &a, const vertex& b)
    {
      iterator pa = find(a);
      if (pa == end())
        return;
      iterator pb = find(b);
      if (pb == end())
        return;
      remove_edge( pa, pb );
    }

    virtual void remove_vertex(iterator pa)
    {
      vertex_set  out_edges = out_neighbors(pa);
      vertex_set  in_edges =  in_neighbors(pa);

      //vertex_set & out_edges = out_neighbors(pa);
      //vertex_set & in_edges =  in_neighbors(pa);

      // remove out-going edges
      for (typename vertex_set::iterator p = out_edges.begin();
                  p!=out_edges.end(); p++)
      {
          remove_edge(pa, find(*p));
      }


      // remove in-coming edges
      for (typename vertex_set::iterator p = in_edges.begin();
                  p!=in_edges.end(); p++)
      {
          remove_edge(find(*p), pa);
      }


      G_.erase(node(pa));
      erase_final(node(pa));
    }


    virtual void remove_vertex_set(const vertex_set &V)
    {
        for (typename vertex_set::const_iterator p=V.begin(); p!=V.end(); p++)
          remove_vertex(*p);
    }


    virtual void remove_vertex(const vertex &a)
    {
        iterator pa = find(a);
        if (pa != G_.end())
            remove_vertex( pa);

    }


   bool includes_vertex(const vertex &a) const
   {
        return  (find(a) != G_.end());
   }

   bool includes_edge(const vertex &a, const vertex &b) const
   {
      return (includes_vertex(a)) ?
          includes_elm(out_neighbors(a),b): false;
   }
   bool includes_elm(const vertex_set& va,const vertex &b ) const
   {
      for(const vertex & v : va)
      {
          if(v==b)
            return true;
      }
      return false;
   }

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< STATIC >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    static const vertex &node(const_iterator p)
    {
        return p->first;
    }

    static const vertex &node(iterator p)
    {
        return p->first;
    }

    static const vertex &node( const_vertex_iterator p)
    {
      return *p;
    }


    static const vertex_set & in_neighbors(const_iterator p)
        { return (p->second).first; }

    static    vertex_set & in_neighbors(iterator p)
          { return (p->second).first; }

    static const_vertex_iterator in_begin(const_iterator p)
    {
        return in_neighbors(p).begin();
    }


    static const_vertex_iterator in_end(const_iterator p)
    {
        return in_neighbors(p).end();
    }


    static const vertex_set& out_neighbors(const_iterator p)
          { return (p->second).second; }

    static const_vertex_iterator out_begin(const_iterator p)
    {
        return out_neighbors(p).begin();
    }


    static const_vertex_iterator out_end(const_iterator p)
    {
        return out_neighbors(p).end();
    }


    static     vertex_set& out_neighbors(iterator p)
          { return (p->second).second; }

    static vertex_iterator out_begin(iterator p)
    {
        return out_neighbors(p).begin();
    }


    static  unsigned int num_edges(const_iterator p)
      {
         return out_neighbors(p).size();
      }

    inline static  unsigned int num_edges(iterator p)
      {
         return out_neighbors(p).size();
      }

    /**
        @param p Agraphe::const_iterator
        @return number of edges going out (out-degree) at node pointed to by p.
    */
    inline static  unsigned int out_degree(const_iterator p)
      {
        return (p->second).second.size();
      }

    inline static  unsigned int out_degree(iterator p)
      {
        return (p->second).second.size();
      }

    /**
        @param p Agraphe::const_iterator
        @return number of edges going out (out-degree) at node pointed to by p.
    */
    inline static  unsigned int in_degree(const_iterator p)
      {
        return (p->second).first.size();
      }

    inline static  unsigned int in_degree(iterator p)
      {
        return (p->second).first.size();
      }

    inline static  unsigned int degree(const_iterator p)
      {
         return in_neighbors(p).size() + out_neighbors(p).size();
      }

    inline static  unsigned int degree(iterator p)
      {
         return in_neighbors(p).size() + out_neighbors(p).size();
      }


    inline static bool isolated(const_iterator p)
    {
        return (in_degree(p) == 0 && out_degree(p) == 0 );
    }

    static bool isolated(iterator p)
    {
        return (in_degree(p) == 0 && out_degree(p) == 0 );
    }
};

std::ostream&operator<<(std::ostream& os, const Agraphe::edge& e);
Agraphe::echantillon_mult get_echantillon_mult(std::istream &is);

#endif
