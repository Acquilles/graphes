#include "Agraphe.hpp"
void Agraphe::write_dot(std::string filename,std::string name,bool erase)const
{
    /// - Si erase est activé, on écrase le fichier
    std::ofstream file;
    if(erase)
        file.open(filename);
    else
        file.open(filename, std::ios::app);

    file << "digraph{\n";
    file << "label=\""<<name<<"\";\n";
    file << "labelloc = \"t\";\n";
    //file << node(begin()) << " [label = "<<name<<"]" << std::endl;
    for (const_iterator p = begin(); p != end(); p++)
    {
        if (out_neighbors(p).size() == 0  &&
            in_neighbors(p).size() == 0)
        {
          file << "\""<<show_vertex(node(p)) << "\";\n";
        }

        if(is_final(node(p)))
        {
            file <<"\""<< show_vertex(node(p)) << "\" [shape = doublecircle]" << std::endl;
        }
    }
    for (const_iterator p = begin(); p != end(); p++)
    {
        const vertex_set &p_out = out_neighbors(p);
        vertex from = node(p);
        for (typename vertex_set::iterator q = p_out.begin();q != p_out.end(); q++)
        {
             for(const lettre&l : get_lettres(from,*q))
                file << "\"" << show_vertex(from) << "\"->\"" << show_vertex(node(q)) << "\"[label =\""<<show_lien(from,node(q),l)<<"\"] ;\n";
        }
    }
    file << "}\n";
    file.close();
}

std::string Agraphe::show_mot(const mot&m)const
{
    std::ostringstream os;
    for(const lettre&l : m)
    {
        os << show_lettre(l);
    }
    return os.str();
}

std::ostream&operator<<(std::ostream& os, const Agraphe::edge& e)
{
    int a,b;
    std::tie(a,b) = e;
    os << a << "->" << b;
    return os;
}

std::ostream & operator<<(std::ostream &s, const typename Agraphe::vertex_set &vs)
{
    for(const typename Agraphe::vertex & v : vs)
    {
        s << v << "  ";
    }
    s << std::endl;
    return s;
}
