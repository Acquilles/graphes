#ifndef _AGRAFREQ_H_
#define _AGRAFREQ_H_

#include "Agraphe.hpp"
#include "Agraprob.hpp"

class Agrafreq : public Agraphe
{
public:
    typedef std::tuple<vertex,vertex,lettre> edge_lettre ;
    typedef std::map<vertex,std::pair<unsigned int, unsigned int >> occurences_vertex;
    typedef std::map<edge_lettre,unsigned int> occurences_edge;

private:
    /// Les occurences de passages et d'arrivée sur chaque état
    occurences_vertex OV_;
    /// Les occurences de passages sur chaque lien
    occurences_edge OE_;

public:
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< CONSTRUCTEURS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    Agrafreq():Agraphe(){}

    Agrafreq(std::istream &is):Agrafreq(get_echantillon_mult(is)){}
    /// Ajout de la construction  du dictionnaire des occurrences
    Agrafreq(const echantillon_mult& positif);
    /// Initialiser les fréquences restantes à partir des partantes
    void intitalize_freq_edge();

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Réécritures >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Ajout de l'initialisation des occurences de v à 0,0
    bool insert_vertex(const vertex &v);
    /// Ajout de l'initialisation des occurences de (a,b)) à 0
    void insert_edge_lettre(const vertex &a, const vertex&b, const lettre&l);

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Occurrences >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Retournes les occurences de v
    unsigned int get_occ(const vertex&v)const;
    /// Retournes le nombre de finaux de v
    unsigned int get_end(const vertex&v)const;
    /// Retournes le nombre de passage par e
    unsigned int get_pass(const edge_lettre&e)const;
    /// Retournes le nombre de passagepar (a,b)
    unsigned int get_pass(const vertex& a,const vertex& b,const lettre&l)const;
    /// Augmentation de l'occurence de v de i
    bool increase_occ(const vertex& v, unsigned int i = 1);
    /// Augmentation du nombre de mots finissants en v de i
    bool increase_end(const vertex& v, unsigned int i = 1);
    /// Augmentation du nombre de mots passant par (a,b)
    bool increase_pass(const edge_lettre& e, unsigned int i = 1);
    /// Augmentation du nombre de mots passant par (a,b)
    bool increase_pass(const vertex& a,const vertex& b, const lettre&l, unsigned int i = 1);
    /// Augmente les occurrences de a de celles de b
    bool fusion_vertex(const vertex&a,const vertex&b);
    /// Augmente les occurrences de a de celles de b
    bool fusion_edge(const vertex&aa,const vertex&ab,const vertex&ba,const vertex&bb,const lettre&l);
    /// Renvoie la fréquence de passage de la lettre l suivant le vertex a
    unsigned int frequence_lettre(const vertex& a, const lettre &l)const;

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ALERGIA >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    // https://grfia.dlsi.ua.es/repositori/grfia/pubs/57/icgi1994.pdf
    /// Renvoie l'ALERGIA
    void to_ALERGIA(double alpha = 0.05,std::string filename = "");

    /// Écrit l'étape de fusion d'ALERGIA dans le filename
    void write_ALERGIA(const vertex&a,const vertex&b, std::string filename);

    /// Indique si deux vertex sont compatibles pour la fusion au sens de l'AERGIA
    bool compatible_ALERGIA(const vertex&a,const vertex&b, double alpha = 0.05)const;

    /// Renvoie faux si on est sur au sens de Hoeffding à un niveau 1-\alpha que les deux fréquences sont différentes
    bool different_Hoeffding(unsigned int fa,unsigned int na,unsigned int fb,unsigned int nb,double alpha = 0.05)const;

    // Renvoie l'arbre des probabilités correspondant
    Agraprob get_agraprob();

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< AFFICHAGE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    // / Ajout de l'affichage des occurrences du vertex
    virtual std::string show_vertex(const vertex&v)const;

    // / Ajout de l'affichage des occurrences du vertex
    virtual std::string show_lien(const vertex&a,const vertex&b,const lettre&l)const;
};

#endif
