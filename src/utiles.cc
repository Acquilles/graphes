#include "utiles.hpp"
#include <cassert>
#define assertm(exp, msg) assert(((void)msg, exp))
double normalCDF(double x) // Phi(-∞, x) aka N(x)
{
    return std::erfc(-x/std::sqrt(2))/2;
}

double normal_pval(double val)
{
    if(val <0) val = -val;
    return normalCDF(-val) + 1 - normalCDF(val);
}

double pval_freq(unsigned int ka,unsigned int na,unsigned int kb,unsigned int nb)
{
    assertm(ka <= na and kb <= nb,"Erreur k > n");
    double fa(0.0),una(1.0);
    double fb(0.0),unb(1.0);

    if(na!=0){
         fa = (double)ka/na;una = fa/na;}
    if(nb!=0){
        fb = (double)kb/nb;unb =fb/nb;}

    double p = (double(ka+kb)/(na+nb));
    if(p*(1-p)==0)
        return 1;
    return normal_pval((fa-fb)/std::sqrt(p*(1-p)*(una+unb)));

}
