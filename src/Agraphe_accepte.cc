#include "Agraphe.hpp"

void Agraphe::set_final(const vertex& v)
{
    set_final(find(v));
}


void Agraphe::set_final(Agraphe::iterator pv)
{
    if(F_.find(node(pv)) == F_.end())
    {
        F_.insert(node(pv));
    }
}


void Agraphe::erase_final(const vertex &v)
{
    if(F_.find(v) != F_.end())
    {
            F_.erase(v);
    }
}


bool Agraphe::is_final(const vertex& v)const
{
    return F_.find(v) != F_.end();
}


bool Agraphe::accepte(const mot& acceptation)const
{
    return accepte(begin(),acceptation,0);
}


bool Agraphe::accepte(const_iterator pa, const mot& acceptation,size_t i)const
{
    if(i == acceptation.size())
    {
        return is_final(node(pa));
    }

    vertex_set a_out = out_neighbors(pa);
    for (vertex_set::iterator pb = a_out.begin(); pb!=a_out.end(); pb++)
    {
        if(is_lettre_in(node(pa),node(pb),acceptation[i]))
        {
            if(accepte(find(*pb),acceptation,i+1))
            {
                return true;
            }
        }
    }
    return false;
}


bool Agraphe::complet(const echantillon& positif)const
{
    for(const mot& m : positif)
    {
        if(!accepte(m))
        {
            return false;
        }
    }
    return true;
}


bool Agraphe::correct(const echantillon& negatif)const
{
    for(const mot& m :negatif)
    {
        if(accepte(m))
        {
            return false;
        }
    }
    return true;
}


bool Agraphe::coherent(const echantillon& positif,const echantillon& negatif)const
{
    return complet(positif) and correct(negatif);
}


/// Renvoie le premier vertex descendant de a sur une lettre l
Agraphe::vertex Agraphe::get_vertex_lettre(const vertex& a, const lettre &l)const
{
    /// DÉTERMINISTE !!!!!!!!!!!!
    for(const vertex& aout : out_neighbors(a))
        if(is_lettre_in(a,aout,l))
            return aout;
    return vertex();
}

Agraphe::vertex Agraphe::get_state(const mot&m)const
{
    return get_state(m, node(begin()),0);
}

Agraphe::vertex Agraphe::get_state(const mot&m,const vertex&v, unsigned int i)const
{
    if(i==m.size())
        return v;
    return get_state(m,get_vertex_lettre(v,m[i]),i+1);
}

std::map<Agraphe::vertex,Agraphe::vertex> Agraphe::get_corr(const Agraphe& agg)
{
    std::map<vertex,vertex> corr;
    recursive_get_corr(agg,corr,node(begin()),node(agg.begin()));
    return corr;
}
void Agraphe::recursive_get_corr(const Agraphe& agg,std::map<vertex,vertex>&corr,const vertex&v,const vertex&v_autre)const
{
    corr[v] = v_autre;
    for(const vertex&vb : out_neighbors(v))
        for(const lettre&l : get_lettres(v,vb))
            recursive_get_corr(agg,corr,vb,agg.get_vertex_lettre(v_autre,l));
}
