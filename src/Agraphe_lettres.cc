#include "Agraphe.hpp"
bool Agraphe::is_lettre_in(const vertex& a, const vertex&b,const lettre& l)const
{
    set_lettre sl = get_lettres(a,b);
    return sl.find(l) != sl.end();
}


/// Renvoie la lettre correspondante à un edge
Agraphe::set_lettre Agraphe::get_lettres(const vertex&a , const vertex&b)const
{
  edge e(a,b);
  if(L_.find(e)==L_.end())
  {
      return Agraphe::set_lettre();
  }
  return L_.at(e);
}


/// Renvoie la lettre correspondante à un edge
Agraphe::set_lettre Agraphe::get_lettres(iterator pa , iterator pb) const
{
  return get_lettres(node(pa),node(pb));
}


/// Lie un edge à une lettre
bool Agraphe::lier_lettre(const vertex&a , const vertex&b,lettre l)
{
  edge e = std::make_pair(a,b);
  if(L_.find(e)==L_.end())
    L_[e] = set_lettre();
  L_[e].insert(l);
  return true;
}


/// Insertion d'un edge avec ses lettres
void Agraphe::insert_edge_lettre(const vertex&a , const vertex&b,const set_lettre& sl)
{
    for(const lettre& l : sl)
        insert_edge_lettre(a,b,l);
}


/// Insertion d'un edge avec sa lettre
void Agraphe::insert_edge_lettre(const vertex&a , const vertex&b,const lettre& l)
{
    insert_edge(a,b);
    lier_lettre(a,b,l);
}

/// Retourne toutes les sortant de ces 2 iterateurs
std::set<Agraphe::lettre> Agraphe::get_lettres_out(const_iterator pa, const_iterator pb)const
{
    // Réunir toutes les letres qui partent des deux vertex
    std::set<lettre> lettres;
    for(const vertex& aout : out_neighbors(pa))
        for(const lettre&l : get_lettres(node(pa),aout))
            lettres.insert(l);
    for(const vertex& bout : out_neighbors(pb))
        for(const lettre&l : get_lettres(node(pb),bout))
            lettres.insert(l);
    return lettres;
}

Agraphe::echantillon_mult get_echantillon_mult(std::istream &is)
{
    Agraphe::echantillon_mult voila;
    std::string line("");
    while(std::getline(is,line))
    {
        Agraphe::mot temp;
        for(size_t i  = 0 ; i < line.size();i++)
        {
            temp.push_back((Agraphe::lettre)(line[i])-48);
        }
        voila.push_back(temp);
    }
    return voila;
}

Agraphe::map_l_to_it Agraphe::get_l_to_it(const vertex&a,const vertex&b)
{
    std::map<lettre,std::pair<iterator,iterator>> l_to_it;

    for(const vertex& ao : out_neighbors(a))
    {
        for(const lettre&l : get_lettres(a,ao))
        {
            l_to_it[l] = std::make_pair(find(ao),end());
        }
    }

    for(const vertex& bo : out_neighbors(b))
    {
        for(const lettre&l : get_lettres(b,bo))
        {
            if(l_to_it.find(l) == l_to_it.end())
                l_to_it[l] = std::make_pair(end(),find(bo));
            else
                l_to_it[l].second = find(bo);
        }
    }

    return l_to_it;
}
