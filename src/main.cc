#include "test.hpp"

void try_alergia_lvr(std::string filerand, std::string filedot,unsigned int N)
{
    // ----------- Former l'échantillon random ------------
    std::ifstream fg(filerand);
    Agraprob a(fg);
    fg.close();
    a.write_dot(filedot,"Probas originales",true);
    Agraphe::echantillon_mult ech;
    for (size_t i = 0; i < N; i++)
        ech.push_back(a.random_mot());

    // --------------------------------------------------

    // ------------ Former l'agraphe -------------------
    Agralergia af(ech);
    // af.write_dot("test_random.dot","MCA",false);
    af.determinize();
    af.set_corr(a);
    std::cout<<"\n\n---- Correspondances ----" <<std::endl;
    for(const std::pair<Agraphe::vertex,Agraphe::vertex>& lala : af.get_corr(a))
    {
        std::cout << lala.first << " = " << lala.second << std::endl;
    }
    std::cout<<"---- FIN Corr----" <<std::endl;
    af.write_dot(filedot,"PTA",false);
    // --------------------------------------------------

    // ------------ CONVERTIR -------------------
    std::cout<<"\n---- CONVERTION " << filerand << " ----" <<std::endl;
    // af.set_lvl_all();
    // af.print_lvl_tree();
    // af.calculer_lvr_all();
    // af.print_lv_tree();
    af.to_alergia_max_lvr(filedot);
    af.write_dot(filedot,"Final",false);
    af.get_agraprob().write_dot(filedot,"Probas",false);
    std::cout<<"---- FIN Convert----" <<std::endl;
    // ------------------------------------------

}

int main(int argc, char** argv)
{
    try_alergia_lvr("random.txt","alergia_lvr_1.dot",50);
    try_alergia_lvr("random2.txt","alergia_lvr_2.dot",50);
}
