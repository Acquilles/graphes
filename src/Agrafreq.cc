#include "Agrafreq.hpp"

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< CONSTRUCTEURS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/// Ajout de la construction  du dictionnaire des occurrences
Agrafreq::Agrafreq(const Agrafreq::echantillon_mult& positif):Agraphe()
{
    vertex compteur(0);
    vertex initial(0);
    compteur++;
    insert_vertex(initial);
    for(const mot& m : positif)
    {
        increase_occ(initial);
        if(m.size()==0)
        {
            increase_end(initial);
            set_final(initial);
        }
        else
        {
            insert_edge_lettre(initial,compteur,m[0]);
            increase_occ(compteur);
            for (size_t i = 1; i < m.size();i++)
            {
                insert_edge_lettre(compteur++,compteur,m[i]);
                increase_occ(compteur);
            }
            increase_end(compteur);
            set_final(compteur++);
        }
    }

    intitalize_freq_edge();
}

void Agrafreq::intitalize_freq_edge()
{
    for(const_iterator pa = begin();pa!=end();pa++)
    {
        for(const vertex&b : out_neighbors(pa))
        {
            for(const lettre&l : get_lettres(node(pa),b))
            {
                increase_pass(node(pa),b,l,get_occ(b));
            }
        }
    }
}

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Réécritures >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
bool Agrafreq::insert_vertex(const vertex &v)
{
    if(Agraphe::insert_vertex(v)){
        OV_[v] = std::make_pair(0,0);
        return true;}
    return false;
}

/// Ajout de l'initialisation des occurences de (a,b)) à 0
void Agrafreq::insert_edge_lettre(const vertex &a, const vertex&b, const lettre&l)
{
    if(is_lettre_in(a,b,l))
        return;
    Agraphe::insert_edge_lettre(a,b,l);
    OE_[edge_lettre(a,b,l)] = 0;
}

/// Augmente les occurrences de a de celles de b
bool Agrafreq::fusion_vertex(const vertex&a,const vertex&b)
{
    if(increase_occ(a,get_occ(b)))
        if(increase_end(a,get_end(b)))
            return true;
    return false;
}

/// Augmente les occurrences de a de celles de b
bool Agrafreq::fusion_edge(const vertex&aa,const vertex&ab,const vertex&ba,const vertex&bb,const lettre&l)
{
    if(increase_pass(aa,ab,l,get_pass(ba,bb,l)))
        return true;
    return false;
}

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Occurrences >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

/// Retournes les occurences de v
unsigned int Agrafreq::get_occ(const vertex&v)const
{
    if(OV_.find(v)!=OV_.end())
        return OV_.at(v).first;
    return 0;
}

/// Retournes le nombre de finaux de v
unsigned int Agrafreq::get_end(const vertex&v)const
{
    if(OV_.find(v)!=OV_.end())
        return OV_.at(v).second;
    return 0;
}

/// Retournes le nombre de passage par (a,b)
unsigned int Agrafreq::get_pass(const edge_lettre&e)const
{
    if(OE_.find(e)!=OE_.end())
        return OE_.at(e);
    return 0;
}
/// Retournes le nombre de passage par e
unsigned int Agrafreq::get_pass(const vertex& a,const vertex& b,const lettre&l)const
{
    return get_pass(edge_lettre(a,b,l));
}
/// Augmentation de l'occurence de v de i
bool Agrafreq::increase_occ(const vertex& v, unsigned int i)
{
    if(OV_.find(v)!=OV_.end()){
        OV_[v].first +=i;
        return true;}
    return false;
}

/// Augmentation du nombre de mots finissants en v de i
bool Agrafreq::increase_end(const vertex& v, unsigned int i)
{
    if(OV_.find(v)!=OV_.end()){
        OV_[v].second +=i;
        return true;}
    return false;
}

/// Augmentation du nombre de mots passant par (a,b)
bool Agrafreq::increase_pass(const vertex& a,const vertex& b,const lettre&l, unsigned int i)
{
    return increase_pass(edge_lettre(a,b,l),i);
}
/// Augmentation du nombre de mots passant par (a,b)
bool Agrafreq::increase_pass(const edge_lettre& e, unsigned int i)
{
    if(OE_.find(e)!=OE_.end())
    {
        OE_[e] +=i;
        return true;
    }

    return false;
}

// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ALERGIA >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

/// Renvoie l'ALERGIA
// https://grfia.dlsi.ua.es/repositori/grfia/pubs/57/icgi1994.pdf
void Agrafreq::to_ALERGIA(double alpha,std::string filename)
{
    // Vecteur des états :
    std::vector<vertex> etats;
    for(const_iterator pv = begin();pv!=end();pv++)
    {
        etats.push_back(node(pv));
    }

    // Essayer chaque possibilité de fusion
    for (size_t i = 0; i < etats.size(); i++)
    {
        for (size_t j = 0; j < i; j++)
        {
            if(includes_vertex(etats[i]) and includes_vertex(etats[j]))
            {
                if(compatible_ALERGIA(etats[j],etats[i],alpha))
                {
                    fusion_deterministe(etats[j],etats[i]);
                    write_ALERGIA(etats[j],etats[i],filename);
                }
            }
        }
    }
}

/// Écrit l'étape de fusion d'ALERGIA dans le filename
void Agrafreq::write_ALERGIA(const vertex&a,const vertex&b, std::string filename)
{
    if(filename != "")
    {
        std::ostringstream nom;
        nom <<'{'<< a << ","<< b << "}";
        write_dot(filename,nom.str(),false);
    }
}

/// Indique si deux vertex sont compatibles pour la fusion au sens de l'AERGIA
bool Agrafreq::compatible_ALERGIA(const vertex&a,const vertex&b, double alpha)const
{
    // DÉTERMINISTE !!!!!!!!!!!!
    if(different_Hoeffding(get_end(a),get_occ(a),get_end(b),get_occ(b),alpha))
        return false;

    // Réunir toutes les letres qui partent des deux vertex
    std::set<lettre> lettres;
    for(const vertex& aout : out_neighbors(a))
        for(const lettre&l : get_lettres(a,aout))
            lettres.insert(l);
    for(const vertex& bout : out_neighbors(b))
        for(const lettre&l : get_lettres(b,bout))
            lettres.insert(l);

    // Tester leur similarité
    for(const lettre& l : lettres)
    {
        // Niquer les 0 parce que ninon pas de récursif :
        unsigned int fa = frequence_lettre(a,l),fb = frequence_lettre(b,l);
        if(different_Hoeffding(fa,get_occ(a),fb,get_occ(b),alpha))
            return false;
        if(fa!=0 and fb != 0)
            if(!compatible_ALERGIA(get_vertex_lettre(a,l),get_vertex_lettre(b,l),alpha))
                return false;
    }

    return true;
}

/// Renvoie la fréquence de passage de la lettre l suivant le vertex a
unsigned int Agrafreq::frequence_lettre(const vertex& a, const lettre &l)const
{
    /// DÉTERMINISTE !!!!!!!!!!!!
    for(const vertex& aout : out_neighbors(a))
        if(is_lettre_in(a,aout,l))
            return get_pass(a,aout,l);
    return 0;
}

/// Renvoie faux si on est sur au sens de Hoeffding à un niveau 1-\alpha que les deux fréquences sont différentes
bool Agrafreq::different_Hoeffding(unsigned int fa,unsigned int na,unsigned int fb,unsigned int nb,double alpha )const
{
    return abs(double(fa)/na-double(fb)/nb) > sqrt(log(2/alpha)*(1/sqrt(na)+1/sqrt(nb))/2);
}

Agraprob Agrafreq::get_agraprob()
{
    Agraprob ap;
    double stay;
    double go;
    vertex a;
    for(const_iterator pa = begin();pa!=end();pa++)
    {
        a = node(pa);
        stay = (double)get_end(a)/get_occ(a);
        if(!ap.set_proba_stay(a,stay))
            ap.insert_vertex(a,stay);
        for(const vertex&b : out_neighbors(pa))
        {
            for(const lettre&l : get_lettres(a,b))
            {
                go = (double)get_pass(a,b,l)/get_occ(a);
                ap.insert_edge_lettre(a,b,l,go);
            }


        }
    }
    return ap;
}
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< AFFICHAGE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/// Ajout de l'affichage des occurrences du vertex
std::string Agrafreq::show_vertex(const vertex&v)const
{
    std::ostringstream s;
    s << v << '[' << get_occ(v) <<',' << get_end(v)<<']';
    return s.str();
}

// / Ajout de l'affichage des occurrences du vertex
std::string Agrafreq::show_lien(const vertex&a,const vertex&b,const lettre&l)const
{
    std::ostringstream s;
    s << show_lettre(l) << '[' << get_pass(a,b,l) <<']';
    return s.str();
}
