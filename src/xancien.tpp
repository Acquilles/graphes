Agraphe::vertex_set_set Agraphe::get_bloc_to_determinize()const
{
    /// Éiminer les cas d'indéterminisme
    vertex_set_set to_merge;
    for (const_iterator p = begin(); p != end(); p++)
    {
        /// - - On réunit les ensembles des vertex descendants associés à leur lettre dans lettre_to_set
        vertex_set p_in = out_neighbors(p);
        std::map<lettre,std::set<vertex>> lettre_to_set;
        for(const vertex& b : p_in)
        {
            set_lettre sl = get_lettres(node(p),b);
            for(const lettre & l : sl)
            {
                if(lettre_to_set.find(l) == lettre_to_set.end())
                {
                    lettre_to_set[l] = std::set<vertex>();
                }
                lettre_to_set[l].insert(b);
            }
        }

        for(const std::pair<lettre,vertex_set>& l_set : lettre_to_set)
        {
            if(l_set.second.size()>1)
            {
                to_merge.insert(l_set.second);
            }
        }
    }
    return to_merge;
}

bool Agraphe::truc_et_astuce(std::set<std::set<vertex>> * to_merge, const vertex_set& v_set)const
{
    for(std::set<std::set<vertex>>::iterator vs = to_merge->begin();vs != to_merge->end();vs++)
    {
        for(const vertex& v : v_set)
        {
            if(vs->find(v)!=vs->end())
            {
                for(const vertex& v : v_set)
                {
                    // unsigned int a(v);
                    vs->clear();
                }
                return true;
            }
        }
    }
    to_merge->insert(v_set);
    return false;
}

bool Agraphe::step_determinize()
{
    return to_quotient(get_bloc_to_determinize());
}
