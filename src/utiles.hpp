#ifndef _UTILES_H
#define _UTILES_H
#include <cmath>
// #include <sqrt>

double normalCDF(double x);

double normal_pval(double val);

double pval_freq(unsigned int ka,unsigned int na,unsigned int kb,unsigned int nb);
#endif
