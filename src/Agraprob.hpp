#ifndef _AGRAPROB_H_
#define _AGRAPROB_H_


#include "Agraphe.hpp"
#include <math.h>
#include <cassert>
#define assertm(exp, msg) assert(((void)msg, exp))

/// class Un graphe comptant les fréquences de passage par les états, pour l'inférence grammaticale stochastique.

class Agraprob : public Agraphe
{

    typedef std::map<vertex,double> proba_vertex;
    typedef std::map<edge,double> proba_edge;

private:

    /// Probabilité de rester sur l'état
    proba_vertex PV_;

    /// Probabilité de passer sur ce lien
    proba_edge PE_;

public:
    Agraprob():Agraphe(){}
    Agraprob(std::istream &is):Agraphe()
    {
        std::string line("");
        vertex a,b;
        lettre l;
        double prob;
        while(std::getline(is,line))
        {
            std::istringstream iss(line);
            iss >> a >> b >> l >> prob;
            insert_edge_lettre(a,b,l,prob);
        }
        maj_stay();
    }

    /// Met à jour les probabilités de rester en fonction des autres
    void maj_stay()
    {
        for(const_iterator pv = begin(); pv!=end();pv++)
        {
            double proba = 1.0;
            for(const vertex& b : out_neighbors(pv))
            {
                proba-=get_proba_lien(node(pv),b);
            }
            assertm(proba >= 0.0, "Probabilités trop élevées");
            set_proba_stay(node(pv),proba);
        }
    }

    bool insert_vertex(const vertex &v,double proba_vertex)
    {
        Agraphe::insert_vertex(v);
        PV_[v] = proba_vertex;
        return true;
    }
    bool insert_vertex(const vertex &v)
    {
        return insert_vertex(v,0.0);
    }

    bool set_proba_stay(const vertex&v, double proba)
    {
        proba_vertex::iterator pv = PV_.find(v);
        if(pv == PV_.end())
            return false;
        PV_[v] = proba;
        return true;

    }
    void insert_edge_lettre(const vertex&a,const vertex&b,const lettre&l, double proba)
    {
        Agraphe::insert_edge_lettre(a,b,l);
        PE_[edge(a,b)] = proba;
    }

    double get_proba_stay(const vertex&a)const
    {
        proba_vertex::const_iterator pv = PV_.find(a);
        if(pv!=PV_.end())
            return pv->second;
        return 0.0;
    }
    double get_proba_lien(const vertex&a,const vertex&b)const
    {
        return get_proba_lien(edge(a,b));
    }
    double get_proba_lien(const edge&e)const
    {
        proba_edge::const_iterator pe = PE_.find(e);
        if(pe!=PE_.end())
            return pe->second;
        return 0.0;
    }

    double get_proba_lettre(const vertex&a,const lettre&l)const
    {
        for(const vertex& b : out_neighbors(a)){
            if(is_lettre_in(a,b,l))
                return get_proba_lien(a,b);
        }
        return 0.0;
    }

    lettre get_lettre(const vertex&a,const vertex&b)const
    {
        return *get_lettres(a,b).begin();
    }

    mot random_mot()
    {
        mot m;
        return random_mot(m,node(begin()));
    }

    mot random_mot(mot& m ,const vertex& a)
    {
        double alea = (double) std::rand()/RAND_MAX;
        for(const vertex& b : out_neighbors(a))
        {
            alea -= get_proba_lien(a,b);
            if(alea < 0)
            {
                m.push_back(get_lettre(a,b))    ;
                return random_mot(m,b);
            }
        }
        return m;
    }

    // / Ajout de l'affichage des occurrences du vertex
    std::string show_vertex(const vertex&v)const
    {
        std::ostringstream s;
        s.precision(2);
        s << v << '[' <<get_proba_stay(v) <<']';
        return s.str();
    }

    // / Ajout de l'affichage des occurrences du vertex
    std::string show_lien(const vertex&a,const vertex&b,const lettre&l)const
    {
        std::ostringstream s;
        s.precision(2);
        s << show_lettre(l) << '[' << get_proba_lien(a,b) <<']';
        return s.str();
    }
};

#endif
